#!/bin/bash
cd ./cmd/antennaserver/
env GOOS=linux GOARCH=arm GOARM=6 go build -o ../../bin/raspberryPi/antennaserver_arm6
env GOOS=linux GOARCH=arm GOARM=7 go build -o ../../bin/raspberryPi/antennaserver_arm7
cd ../../
#GO111MODULE=on fyne-cross linux -arch=amd64,386 -icon="satellite.png" -name="GoHamSatTraker" -release -app-build=1 -app-id="gohamsattraker.net.circuiteando" -app-version="1.0" ./cmd/gohamsattraker
fyne-cross linux -arch=amd64,386 -icon="satellite.png" -name="GoHamSatTraker" -release -app-build=1 -app-id="gohamsattraker.net.circuiteando" -app-version="1.0" ./cmd/gohamsattraker

cp fyne-cross/bin/linux-386/* bin/linux/386
cp fyne-cross/dist/linux-386/* bin/linux/386
cp fyne-cross/bin/linux-amd64/* bin/linux/amd64
cp fyne-cross/dist/linux-amd64/* bin/linux/amd64
#GO111MODULE=on fyne-cross android -arch=arm,arm64 -icon="satellite.png" -name="GoHamSatTraker" -debug -app-build=1 -app-id="gohamsattraker.net.circuiteando" -app-version="1.0" ./cmd/gohamsattraker
fyne-cross android -arch=arm,arm64 -icon="satellite.png" -name="GoHamSatTraker" -app-build=1 -app-id="gohamsattraker.net.circuiteando" -app-version="1.0" ./cmd/gohamsattraker

cp fyne-cross/dist/android-arm/* bin/android/arm
cp fyne-cross/dist/android-arm64/* bin/android/arm64
rm -R fyne-cross
