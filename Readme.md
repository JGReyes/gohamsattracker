GoHAMSatTracker
================

En este repositorio se encuentra el código fuente de los programa en Go para PC, Android y Raspberry Pi para el control de una antena motorizada.

![Siguiendo_satelite](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhDT4rvVdOza8jzgjWnaqkWVrg55zwEw2njr7qd-XI5z-XDPWLY-Z2t9P8fYtnNHGfSgO7x4uLhTf8MZNkvdLgfDu9NEG__yCZ6T2woPGECd6MsCuTPgpIs-kz52FaFJAsr8cqYppZ9vpeit1VV_wGF8UtnstTgysP_0ldoeIdBL5ublmnQ_-c53P1b6g/s819/principal.png)

Las principales características de los programas son las siguientes:

Para PC/Android

- Visualización de posición y velocidad de un satélite seleccionado, así como la hora actual en local y UTC.
- Configuración de los parámetros de los motores.
- Configuración de la posición donde está situada la antena (observador).
- Compensación de la holgura en los engranajes (backlash).
- Configuración de los límites de movimiento de la antena durante el seguimiento automático del satélite.
- Control de los motores de forma manual sin limitaciones.
- Configuración de la apariencia de la aplicación, incluyendo un tema claro y otro oscuro.
- Visualización del voltaje de la batería, así como una alarma configurable por bajo voltaje.
- Actualización los datos TLE para el cálculo de órbitas directamente desde la página web de la NASA.
- Búsqueda de un satélite entre los descargados en el proceso anterior.
- Mostrar el log del programa para solucionar posibles errores.
- Mandar comandos de control y recibir datos de la antena (Raspberry Pi).
- Apagado de los motores de forma remota.
- Interfaz en dos idiomas: inglés y español.
- Pruebas para comprobar el correcto funcionamiento de los motores.

Para Raspberry Pi Zero W

- Recibir comandos del programa de PC/Android para movimiento de los motores.
- Calcular la órbita de un satélite seleccionado en la aplicación y mandar de forma periódica los datos de posición de vuelta a la misma.
- Capacidad de operar autónomamente en caso de desconexión de la aplicación durante el seguimiento de un satélite.
- Transmisión de los datos en modelo publicador/subscriptor usando NATS.
- Datos de los mensajes en formato binario compacto CBOR. Lo que lleva a comunicaciones más cortas y con comprobación de integridad.
  Antes de realizar cualquier tarea, se comprueba que los datos recibidos sean válidos.
- Posibilidad de posicionar los motores en pasos, en vueltas (con decimales) o en ángulo.
- Sincronizarse con la hora del programa al iniciarse la conexión.
- Utilizar múltiples hilos de ejecución para paralelizar las tareas.

Para más información consultar el artículo sobre software en el [blog.](https://www.circuiteando.net/2023/05/hamsattracker-parte-4-software.html)
