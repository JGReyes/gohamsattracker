#!/bin/bash
cd ./bin/raspberryPi/
echo scp antennaserver_arm6 natsServer.conf ../../antennaserver.service pi@10.0.0.5:antennaserver
scp antennaserver_arm6 natsServer.conf ../../antennaserver.service pi@10.0.0.5:antennaserver

echo 'Run in raspberrypi ssh shell (to boot at startup):'
echo 'sudo cp /home/pi/antennaserver/antennaserver.service /lib/systemd/system'
echo 'sudo chmod 644 /lib/systemd/system/antennaserver.service'
echo 'sudo systemctl enable antennaserver.service'
echo 'sudo reboot'
echo 'To check: sudo systemctl status antennaserver.service'
echo 'Do not forget enable I2C bus with sudo raspi-config and option 3'
