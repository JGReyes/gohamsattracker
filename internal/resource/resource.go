/*
File: resource.go
Project: GoHAMSatTracker
Created Date: Friday, June 10th 2022, 12:24:39 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Recursos estáticos de la aplicación
package resource

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
)

type tVar int64

const (
	undefined tVar = iota
	dark
	light
)

var themeVariant tVar

// SetResVariant obtiene el tema de la aplicación (oscuro o claro) para
// seleccionar los iconos correspondiente.
// Debe llamarse antes de crear la UI.
func SetResVariant() {
	t := fyne.CurrentApp().Settings().ThemeVariant()
	if t == theme.VariantDark {
		themeVariant = dark
	} else if t == theme.VariantLight {
		themeVariant = light
	} else {
		themeVariant = undefined
	}
}

func LangES() *fyne.StaticResource {
	return rActiveEsToml
}

func LanguageIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rLanguagewhite24dpSvg
	}
	return rLanguageblack24dpSvg
}

func AppearanceIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rPictureinpicturewhite24dpSvg
	}
	return rPictureinpictureblack24dpSvg
}

func ControlIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rSettingsremotewhite24dpSvg
	}
	return rSettingsremoteblack24dpSvg
}

func LogIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rArticlewhite24dpSvg
	}
	return rArticleblack24dpSvg
}

func WifiOffIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rWifioffwhite24dpSvg
	}
	return rWifioffblack24dpSvg
}

func WifiOnIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rWifiwhite24dpSvg
	}
	return rWifiblack24dpSvg
}

func RedoIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rRedowhite24dpSvg
	}
	return rRedoblack24dpSvg
}

func UndoIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rUndowhite24dpSvg
	}
	return rUndoblack24dpSvg
}

func RefreshIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rRefreshwhite24dpSvg
	}
	return rRefreshblack24dpSvg
}

func ArrowUpIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rArrowupwardwhite24dpSvg
	}
	return rArrowupwardblack24dpSvg
}

func ArrowLeftIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rArrowbackwhite24dpSvg
	}
	return rArrowbackblack24dpSvg
}

func ArrowRightIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rArrowforwardwhite24dpSvg
	}
	return rArrowforwardblack24dpSvg
}

func ArrowDownIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rArrowdownwardwhite24dpSvg
	}
	return rArrowdownwardblack24dpSvg
}

func LocationIcon() *fyne.StaticResource {
	if themeVariant == dark {
		return rLocationonwhite24dpSvg
	}
	return rLocationonblack24dpSvg
}
