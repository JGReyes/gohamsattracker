/*
File: msg.go
Project: GoHAMSatTracker
Created Date: Monday, June 13th 2022, 6:31:24 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene todos los mensajes de la UI para su posterior traducción.
package sattrk

import (
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

var msgLatitude = &i18n.Message{
	ID:    "latitude",
	Other: "Latitude:",
}

var msgLongitude = &i18n.Message{
	ID:    "longitude",
	Other: "Longitude:",
}

var msgDistance = &i18n.Message{
	ID:    "distance",
	Other: "Distance:",
}

var msgPeriod = &i18n.Message{
	ID:    "period",
	Other: "Period:",
}

var msgAzimuth = &i18n.Message{
	ID:    "azimuth",
	Other: "Azimuth:",
}

var msgElevation = &i18n.Message{
	ID:    "elevation",
	Other: "Elevation:",
}

var msgAltitude = &i18n.Message{
	ID:    "altitude",
	Other: "Altitude:",
}

var msgSpeed = &i18n.Message{
	ID:    "speed",
	Other: "Speed:",
}

var msgTime = &i18n.Message{
	ID:    "time",
	Other: "Time:",
}

var msgStart = &i18n.Message{
	ID:    "start",
	Other: "Start",
}

var msgStop = &i18n.Message{
	ID:    "stop",
	Other: "Stop",
}

var msgTrack = &i18n.Message{
	ID:    "track",
	Other: "Track",
}

var msgTracking = &i18n.Message{
	ID:    "tracking",
	Other: "Tracking...",
}

var msgStatus = &i18n.Message{
	ID:    "status",
	Other: "Status:",
}

var msgConfiguration = &i18n.Message{
	ID:    "configuration",
	Other: "Configuration",
}

var msgAppearance = &i18n.Message{
	ID:    "appearance",
	Other: "Appearance",
}

var msgControl = &i18n.Message{
	ID:    "control",
	Other: "Control",
}

var msgProgram = &i18n.Message{
	ID:    "program",
	Other: "Program",
}

var msgAmsat = &i18n.Message{
	ID:    "amsat",
	Other: "Amsat Url:",
}

var msgDirectory = &i18n.Message{
	ID:    "directory",
	Other: "Data directory:",
}

var msgHost = &i18n.Message{
	ID:    "host",
	Other: "Host IP:",
}

var msgInterval = &i18n.Message{
	ID:    "interval",
	Other: "Interval (ms):",
}

var msgObserver = &i18n.Message{
	ID:    "observer",
	Other: "Observer location",
}

var msgLocal = &i18n.Message{
	ID:    "local",
	Other: "Local",
}

var msgMotor = &i18n.Message{
	ID:    "motor",
	Other: "Motor",
}

var msgMinRange = &i18n.Message{
	ID:    "minRange",
	Other: "Min. Range (degrees):",
}

var msgMaxRange = &i18n.Message{
	ID:    "maxRange",
	Other: "Max. Range (degrees):",
}

var msgSteps = &i18n.Message{
	ID:    "steps",
	Other: "Steps per revolution:",
}

var msgGears = &i18n.Message{
	ID:    "gears",
	Other: "Gears multiplicator:",
}

var msgEnable = &i18n.Message{
	ID:    "enable",
	Other: "Enable",
}

var msgClose = &i18n.Message{
	ID:    "close",
	Other: "Close",
}

var msgDownload = &i18n.Message{
	ID:    "download",
	Other: "Download TLS data",
}

var msgSelect = &i18n.Message{
	ID:    "select",
	Other: "Select one satellite",
}

var msgMElevation = &i18n.Message{
	ID:    "mElevation",
	Other: "Elevation",
}

var msgMAzimuth = &i18n.Message{
	ID:    "mAzimuth",
	Other: "Azimuth",
}

var msgMRotation = &i18n.Message{
	ID:    "mRotation",
	Other: "Rotation",
}

var msgOn = &i18n.Message{
	ID:    "on",
	Other: "ON",
}

var msgOff = &i18n.Message{
	ID:    "off",
	Other: "OFF",
}

var msgLanguage = &i18n.Message{
	ID:    "language",
	Other: "Language",
}

var msgSettings = &i18n.Message{
	ID:    "settings",
	Other: "Settings",
}

var msgLog = &i18n.Message{
	ID:    "log",
	Other: "Log",
}

var msgStepSd = &i18n.Message{
	ID:    "steps",
	Other: "Steps: ",
}

var msgRight = &i18n.Message{
	ID:    "right",
	Other: "Right",
}

var msgLeft = &i18n.Message{
	ID:    "left",
	Other: "Left",
}

var msgUp = &i18n.Message{
	ID:    "up",
	Other: "Up",
}

var msgDown = &i18n.Message{
	ID:    "down",
	Other: "Down",
}

var msgObsElevation = &i18n.Message{
	ID:    "obsElevation",
	Other: "Elevation(m):",
}

var msgEnableInv = &i18n.Message{
	ID:    "enableInv",
	Other: "Inverted",
}

var msgInitPos = &i18n.Message{
	ID:    "initPos",
	Other: "Initial position",
}

var msgBacklash = &i18n.Message{
	ID:    "backlash",
	Other: "Backlash",
}

var msgBattery = &i18n.Message{
	ID:    "battery",
	Other: "Low battery:",
}

var msgBatteryVolt = &i18n.Message{
	ID:    "batteryVolt",
	Other: "Battery:",
}

var msgBlsAzimuth = &i18n.Message{
	ID:    "blsAzimuth",
	Other: "Azimuth (steps):",
}

var msgBlsElevation = &i18n.Message{
	ID:    "blsElevation",
	Other: "Elevation (steps):",
}

var msgBlsRotation = &i18n.Message{
	ID:    "blsRotation",
	Other: "Rotation (steps):",
}

var msgMLimits = &i18n.Message{
	ID:    "mLimits",
	Other: "Motors limits test",
}

var msg90Pass = &i18n.Message{
	ID:    "90Pass",
	Other: "Simulate 90º pass",
}

var msgDisMotor = &i18n.Message{
	ID:    "disMotor",
	Other: "Disable motors",
}

var msgDisMotorAns = &i18n.Message{
	ID:    "disMotorAns",
	Other: "Are you sure? The antenna will drop from its current position",
}

var msgYes = &i18n.Message{
	ID:    "yes",
	Other: "Yes",
}

var msgNo = &i18n.Message{
	ID:    "no",
	Other: "No",
}
