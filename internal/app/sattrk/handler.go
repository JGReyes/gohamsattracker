/*
File: handler.go
Project: GoHAMSatTracker
Created Date: Tuesday, June 14th 2022, 1:20:22 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene los manejadores de todos los eventos de la interfaz de usuario.
package sattrk

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/widget"
	"github.com/fxamacker/cbor/v2"
	"github.com/nats-io/nats.go"

	"GoHAMSatTracker/internal/pkg/core"
)

// Select personalizado para realizar busquedas.
type searchSelect struct {
	widget.SelectEntry
}

var watchdogFeed = make(chan bool)
var watchDone = make(chan bool)
var battWarnShow bool = false

func NewSearchSelect(options []string) *searchSelect {
	sl := &searchSelect{}
	sl.ExtendBaseWidget(sl)
	sl.SetOptions(options)
	return sl
}

func (sl *searchSelect) TypedRune(r rune) {

	sl.Entry.TypedRune(r)
	result := []string{}

	for i := range cachedSatNames {
		if strings.HasPrefix(strings.ToLower(cachedSatNames[i]), strings.ToLower(sl.Text)) {
			result = append(result, cachedSatNames[i])
		}
	}

	if len(result) > 0 {
		sl.SetOptions(result)
	}
}

func (sl *searchSelect) FocusLost() {
	if sl.Text == "" {
		sl.SetOptions(cachedSatNames)
	}
}

// sattelliteSelected establece el satélite actualmente seleccionado
func sattelliteSelected(sat string) {
	selectedSat = sat
	if antStatus == Started || antStatus == Tracking {
		startTapped()
	}
}

// langSelected selecciona el lenguaje de la aplicación
func langSelected(name string) {
	binds.language.Set(name)
	selectedMotor = localize(msgMElevation)
}

// battDataRecv muestra y controla el voltage de la batería cuando la recibe de la antena
func battDataRecv(msg *nats.Msg) {
	var batt float32
	var dlg dialog.Dialog
	if err := cbor.Unmarshal(msg.Data, &batt); err == nil {
		MainWidgets.battery.SetText(fmt.Sprintf("%.2f V", batt))
		lowBat, err := strconv.ParseFloat(getBind("battWarn"), 32)
		if err == nil && batt < float32(lowBat) && !battWarnShow {
			win := fyne.CurrentApp().Driver().AllWindows()[0]
			dlg = dialog.NewInformation("Low Battery",
				"Battery voltage below "+getBind("battWarn")+" volts", win)
			dlg.SetOnClosed(disMotorTapped)
			dlg.Show()
			battWarnShow = true
		}
	}
}

// trackDataRecv muestra los datos de seguimiento del satélite cuando los recibe de la antena
func trackDataRecv(msg *nats.Msg) {
	var data core.TrackingData

	// Se alimenta al watchdog sin bloquear.
	select {
	case watchdogFeed <- true:
	case <-time.After(10 * time.Millisecond): // Timeout
	}

	if cbor.Unmarshal(msg.Data, &data) == nil {
		alt, vel, pos := core.LatLongFromVector(data.SatPosition, data.Gmst)
		elDeg := data.SatAngles.El * (180 / math.Pi)
		azDeg := data.SatAngles.Az * (180 / math.Pi)
		period := core.SatPeriod(alt)
		velKm := vel * 3600 // km/h

		MainWidgets.distance.SetText(fmt.Sprintf("%.2f km", data.SatAngles.Rg))
		MainWidgets.altitude.SetText(fmt.Sprintf("%.2f km", alt))
		MainWidgets.speed.SetText(fmt.Sprintf("%.2f km/s - %.0f km/h", vel, velKm))
		MainWidgets.latitude.SetText(fmt.Sprintf("%.5f", pos.Latitude))
		MainWidgets.longitude.SetText(fmt.Sprintf("%.5f", pos.Longitude))
		MainWidgets.azimuth.SetText(fmt.Sprintf("%.2f º", azDeg))
		MainWidgets.elevation.SetText(fmt.Sprintf("%.2f º", elDeg))
		MainWidgets.period.SetText(fmt.Sprintf("%.2f min.", period))
	}
}

// startTapped inicia la conexión y el proceso de cálculo en la Raspberry Pi de la antena
func startTapped() {

	if selectedSat != "" {
		// Obtiene los datos del observador de la configuración
		long := getBind("obsLongitude")
		lat := getBind("obsLatitude")
		elev := getBind("obsElevation")

		if long == "" || lat == "" || elev == "" {
			win := fyne.CurrentApp().Driver().AllWindows()[0]
			dialog.NewInformation("No observer location",
				"Fill the observer's fields in the configuration", win).Show()
			return
		}

		if antStatus == Disconnect {
			core.Connection = core.Connect(pref.String("host"))
		} else {
			if antStatus == Tracking {
				trackTapped()
			}
			core.Publish(core.Subs.Motors, []byte("STOPCALC"), "")
			time.Sleep(500 * time.Millisecond)
			initPosTapped()
			core.Connection.Close()
			core.Connection = nil
		}

		if core.Connection != nil {
			log.Printf("SatTracker's client connected")
			setStatus(status, true, false, false)

			// Esta función se encarga de comprobar si se reciben datos desde la
			// antena. De no transcurrir en el intervalo configurado 5 veces, lanza un error.
			go func() {
				interval, _ := strconv.ParseUint(getBind("interval"), 10, 16)
				var feed bool = false
				check := time.NewTicker(time.Duration(interval) * 5 * time.Millisecond)

				for {
					select {
					case <-check.C:
						if !feed {
							log.Println("Error in antenna connection.Timeout")
							win := fyne.CurrentApp().Driver().AllWindows()[0]
							dialog.NewInformation("Error", "Error in antenna connection", win).Show()
							check.Stop()
							//startTapped() // Intenta para la antena al no recibir datos.
							return
						} else {
							feed = false
						}
					case <-watchdogFeed:
						feed = true
					case <-watchDone:
						check.Stop()
						return
					}
				}
			}()

		} else {
			log.Printf("SatTracker's client Disconnect")
			setStatus(status, false, false, false)
			battWarnShow = false
			clearData()
			// Al desconectarse se para el watchdog.
			select {
			case watchDone <- true:
			case <-time.After(500 * time.Millisecond):
			}
			return
		}

		core.Subscribe(core.Subs.Preferences, func(msg *nats.Msg) {
			if string(msg.Data) == "PrefsAck" {
				setStatus(status, true, true, false)
			}
		})

		core.Subscribe(core.Subs.Battery, battDataRecv)
		core.Subscribe(core.Subs.TrackData, trackDataRecv)

		tleFile := pref.String("DATA_DIR") + pref.String("TLEFilename")
		resp := core.SetSat(selectedSat, tleFile)
		if !resp {
			win := fyne.CurrentApp().Driver().AllWindows()[0]
			log.Println("Error at creation of satellite object")
			dialog.NewInformation("Error", "Error at creation of satellite object", win).Show()
			setStatus(status, true, false, false)
		} else {
			configData := prefsToStruct()
			line1, line2 := core.GetTLELines()
			configData.TLEline1 = line1
			configData.TLEline2 = line2
			configData.Time = time.Now().Format(time.RFC3339)
			data, err := cbor.Marshal(configData)

			if err != nil {
				log.Println("Error at encoding preferences in to CBOR")
			} else {
				core.Publish(core.Subs.Preferences, data, "PrefsAck")
				log.Println("Sended preferences to antenna")
			}
		}
	} else {
		win := fyne.CurrentApp().Driver().AllWindows()[0]
		dialog.NewInformation("Not satellite selected", "Select a satellite first", win).Show()
	}
}

// trackTapped inicia/para el movimiento de la antena hacia el satélite seleccionado
func trackTapped() {
	if antStatus == Started {
		log.Printf("Start tracking pressed")
		core.Publish(core.Subs.Motors, []byte("TRACK"), "")
		setStatus(status, true, true, true)
	} else if antStatus == Tracking {
		log.Printf("Stop tracking pressed")
		core.Publish(core.Subs.Motors, []byte("STOPTRACK"), "")
		setStatus(status, true, true, false)
	}
}

// motorEnableTapped cambia el estado que indica que un motor está habilitado
func motorEnableTapped(enable string) {
	switch selectedMotor {
	case localize(msgMElevation):
		if enable == localize(msgOn) {
			binds.enableEl.Set(true)
		} else {
			binds.enableEl.Set(false)
		}
	case localize(msgMAzimuth):
		if enable == localize(msgOn) {
			binds.enableAz.Set(true)
		} else {
			binds.enableAz.Set(false)
		}
	case localize(msgMRotation):
		if enable == localize(msgOn) {
			binds.enableRo.Set(true)
		} else {
			binds.enableRo.Set(false)
		}
	}
}

// motorEnInvTapped cambia el estado que indica si el pin enable del motor está invertido
func motorEnInvTapped(value bool) {
	switch selectedMotor {
	case localize(msgMElevation):
		if value {
			binds.enableInvEl.Set(true)
		} else {
			binds.enableInvEl.Set(false)
		}
	case localize(msgMAzimuth):
		if value {
			binds.enableInvAz.Set(true)
		} else {
			binds.enableInvAz.Set(false)
		}
	case localize(msgMRotation):
		if value {
			binds.enableInvRo.Set(true)
		} else {
			binds.enableInvRo.Set(false)
		}
	}
}

// closeTapped cierra la pantalla de configuración y salva las preferencias
func closeTapped() {
	if configVisible {
		savePreferences()
		configVisible = false
		loadPreferences()
	}

	content.RemoveAll()
	content.Add(makeMain())
	content.Refresh()
}

// stopTapped para todos los motores
func stopTapped() {
	core.Publish(core.Subs.Motors, []byte("STOP"), "")
	setStatus(status, true, true, false)
}

// initPosTapped devuelve a la antena a su posición inicial
func initPosTapped() {
	core.Publish(core.Subs.Motors, []byte("INITPOS"), "")
}

// closeLogTapped cierra la pantalla de log
func closeLogTapped() {

	content.RemoveAll()
	content.Add(makeMain())
	content.Refresh()
}

// downloadTapped descarga el archivo con datos TLE de los satélites.
func downloadTapped() {
	url := pref.String("AMSAT_URL")
	dirUri := pref.String("DATA_DIR")
	dir, uriErr := storage.ParseURI(dirUri)

	win := fyne.CurrentApp().Driver().AllWindows()[0]

	if uriErr != nil {
		dialog.NewError(uriErr, win).Show()
		return
	}

	fname, err := core.DownloadTLE(url, dir.Path())

	if err != nil {
		if err.IsOfType(core.ConnectError) {
			dialog.NewError(err, win).Show()
		} else {
			log.Printf("Error: %+v", err)
		}
		return
	}

	pref.SetString("TLEFilename", fname)
	dialog.NewInformation("Download succefully", "TLE data download finished", win).Show()
}

// rightTapped mueve la antena hacia la derecha
func rightTapped() {
	core.SendSteps(core.MotorSteps{StepsAz: controlStepsAz})
}

// leftTapped mueve la antena hacia la izquierda
func leftTapped() {
	core.SendSteps(core.MotorSteps{StepsAz: -(controlStepsAz)})
}

// upTapped meve la antena hacia arriba
func upTapped() {
	core.SendSteps(core.MotorSteps{StepsEl: controlStepsEl})
}

// downTapped mueve la antena hacia abajo
func downTapped() {
	core.SendSteps(core.MotorSteps{StepsEl: -(controlStepsEl)})
}

// rotRightTapped rota la antena hacia la derecha
func rotRightTapped() {
	core.SendSteps(core.MotorSteps{StepsRo: controlStepsRo})
}

// rotLeftTapped rota la antena hacia la izquierda
func rotLeftTapped() {
	core.SendSteps(core.MotorSteps{StepsRo: -(controlStepsRo)})
}

// motorsLimitsTapped gira todos los motores más de una vuelta para probar
// los límites indicados en la configuración
func motorsLimitsTapped() {
	core.Publish(core.Subs.Motors, []byte("TESTLIMITS"), "")
}

// pass90Tapped() gira todos los motores simulando un pase a 90 grados de elevación
func pass90Tapped() {
	core.Publish(core.Subs.Motors, []byte("TEST90"), "")
}

// disMotorConfirm es llamada cuando el usuario responde a la confirmación de apagar motores.
func disMotorConfirm(reply bool) {
	if reply {
		core.Publish(core.Subs.Motors, []byte("DISMOTORS"), "")
	}
}

// disMotorTapped() mueve la antena a su posición más baja y deshabilita los motores.
func disMotorTapped() {
	win := fyne.CurrentApp().Driver().AllWindows()[0]
	confDlg := dialog.NewConfirm(localize(msgDisMotor), localize(msgDisMotorAns),
		disMotorConfirm, win)
	confDlg.SetConfirmText(localize(msgYes))
	confDlg.SetDismissText(localize(msgNo))
	confDlg.Show()
}

// clearData borra los datos del satélite en pantalla
func clearData() {
	empty := "__________"
	MainWidgets.altitude.SetText(empty)
	MainWidgets.azimuth.SetText(empty)
	MainWidgets.distance.SetText(empty)
	MainWidgets.elevation.SetText(empty)
	MainWidgets.latitude.SetText(empty)
	MainWidgets.longitude.SetText(empty)
	MainWidgets.period.SetText(empty)
	MainWidgets.speed.SetText(empty)
	MainWidgets.battery.SetText(empty)
}
