/*
File: theme.go
Project: GoHAMSatTracker
Created Date: Tuesday, June 14th 2022, 11:28:03 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene la personalización del tema por defecto de la aplicación.
package sattrk

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
)

type myTheme struct {
}

func (t *myTheme) Size(n fyne.ThemeSizeName) float32 {
	dev := fyne.CurrentDevice()
	if n == theme.SizeNamePadding {
		if dev.IsMobile() {
			return 5
		} else {
			return 3
		}
	}
	if n == theme.SizeNameText {
		if dev.IsMobile() {
			return 14
		} else {
			return 12
		}
	}
	return theme.DefaultTheme().Size(n)
}

func (t *myTheme) Font(n fyne.TextStyle) fyne.Resource {
	return theme.DefaultTheme().Font(n)
}

func (t *myTheme) Color(n fyne.ThemeColorName, v fyne.
	ThemeVariant) color.Color {
	return theme.DefaultTheme().Color(n, v)
}

func (t *myTheme) Icon(n fyne.ThemeIconName) fyne.Resource {
	return theme.DefaultTheme().Icon(n)
}
