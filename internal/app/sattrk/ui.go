/*
File: ui.go
Project: GoHAMSatTracker
Created Date: Tuesday, June 7th 2022, 1:57:27 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene toda la interfaz de usuario de la aplicación.
package sattrk

import (
	"GoHAMSatTracker/internal/pkg/core"
	"GoHAMSatTracker/internal/pkg/validator"
	"GoHAMSatTracker/internal/resource"
	"fmt"
	"math"
	"strconv"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/cmd/fyne_settings/settings"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
)

// Contenido del encabezado.
var header *fyne.Container

// Contenido de la barra de estado.
var status *fyne.Container

// Contenido principal de la aplicación.
var content *fyne.Container
var bundle *i18n.Bundle
var localizer *i18n.Localizer

// Barra de herramientas
var toolbar *widget.Toolbar

// Ajustes de la aplicación guadados de forma permanente.
type mSettings struct {
	set       *settings.Settings
	isVisible bool
}

// Binds del framework Fyne para los campos de la configuración.
type prefBindings struct {
	amsat        binding.String
	directory    binding.String
	host         binding.String
	interval     binding.String
	obsLatitude  binding.String
	obsLongitude binding.String
	obsElevation binding.String
	minRangeEl   binding.String
	maxRangeEl   binding.String
	stepsRevEl   binding.String
	gearsMulEl   binding.String
	speedEl      binding.String
	enableEl     binding.Bool
	enableInvEl  binding.Bool
	minRangeAz   binding.String
	maxRangeAz   binding.String
	stepsRevAz   binding.String
	gearsMulAz   binding.String
	speedAz      binding.String
	enableAz     binding.Bool
	enableInvAz  binding.Bool
	minRangeRo   binding.String
	maxRangeRo   binding.String
	stepsRevRo   binding.String
	gearsMulRo   binding.String
	speedRo      binding.String
	enableRo     binding.Bool
	enableInvRo  binding.Bool
	language     binding.String
	backLsEl     binding.String
	backLsAz     binding.String
	backLsRo     binding.String
	battWarn     binding.String
}

// Contiene los punteros a los campos rellenables del menú principal
type mainWidgets struct {
	latitude  *widget.Label
	longitude *widget.Label
	distance  *widget.Label
	period    *widget.Label
	azimuth   *widget.Label
	elevation *widget.Label
	altitude  *widget.Label
	speed     *widget.Label
	locTime   *widget.Label
	utcTime   *widget.Label
	startBtn  *widget.Button
	trackBtn  *widget.Button
	battery   *widget.Label
}

type Status uint

const (
	Disconnect Status = iota
	Connected
	Started
	Tracking
)

var antStatus Status
var MainWidgets mainWidgets
var mySettings mSettings
var configVisible = false
var binds *prefBindings
var selectedMotor string
var selectedSat string
var cachedSatNames []string
var pref fyne.Preferences
var controlStepsEl int
var controlStepsAz int
var controlStepsRo int

// initTranslations inicializa el lenguaje de la aplicación.
func initTranslations() {
	bundle = i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)
	langEsFile := resource.LangES()
	bundle.ParseMessageFileBytes(langEsFile.Content(), langEsFile.Name())
	localizer = i18n.NewLocalizer(bundle, language.English.String())
}

// MakeUI crea todo el contenido de la interzar de usuario.
func MakeUI(w fyne.Window) *fyne.Container {
	pref = fyne.CurrentApp().Preferences()
	binds = makeBindings()
	loadPreferences()
	dev := fyne.CurrentDevice()
	mySettings = mSettings{set: settings.NewSettings(), isVisible: false}
	resource.SetResVariant()
	initTranslations()
	fyne.CurrentApp().Settings().SetTheme(&myTheme{})
	core.LogToFile(pref.String("DATA_DIR") + "log.txt")
	selectedMotor = localize(msgMElevation)

	toolbar = widget.NewToolbar()
	// Si el menú ya es visible, se cierra y se carga la pantalla principal.
	settingsMenu := func() {
		if mySettings.isVisible {
			content.RemoveAll()
			mySettings.isVisible = false
			content.Add(makeMain())
			content.Refresh()
			if !dev.IsMobile() {
				w.SetFixedSize(false)
			} else {
				w.SetFullScreen(true)
				w.SetFixedSize(true)
			}
		}

		resource.SetResVariant()
		// Menú principal.
		pm := widget.NewPopUpMenu(makePopMenu(w), w.Canvas())
		pm.Resize(fyne.NewSize(pm.Size().Width, w.Canvas().Size().Height))
		pm.Show()

		// Si no es un dispositivo móvil, el menú se alarga hasta la barra de estado
		// cuando se cambia el tamaño de la ventana.
		if !dev.IsMobile() {
			autoResize := func() {
				for pm.Visible() {
					pmHeight := pm.Size().Height
					wHeight := w.Canvas().Size().Height
					if pmHeight != wHeight {
						pm.Resize(fyne.NewSize(pm.Size().Width, wHeight))
					}
					time.Sleep(100 * time.Millisecond)
				}
			}

			go autoResize()
		}

	}

	if dev.IsMobile() {
		w.SetFullScreen(true)
		w.SetFixedSize(true)
	}

	toolbar.Append(widget.NewToolbarAction(theme.MenuIcon(), settingsMenu))
	header = container.NewHBox(layout.NewSpacer(), makeTitle(), layout.NewSpacer(),
		toolbar)
	status = container.NewHBox()
	content = makeMain()
	setStatus(status, false, false, false)

	// Pone el reloj en marcha
	go func() {
		t := time.NewTicker(1 * time.Second)
		var locTimeS, utcTimeS string
		for {
			<-t.C
			locTimeS = time.Now().Local().Format("15:04:05")
			utcTimeS = time.Now().UTC().Format("15:04:05") + "  (UTC)"
			MainWidgets.locTime.Text = locTimeS
			MainWidgets.utcTime.Text = utcTimeS
			MainWidgets.locTime.Refresh()
			MainWidgets.utcTime.Refresh()
		}
	}()

	return container.NewBorder(header, status, nil, nil, header,
		status, content)
}

// Devuelve la altura disponible para el contenido.
func ContentHeight() float32 {
	return fyne.CurrentApp().Driver().AllWindows()[0].Canvas().Size().Height -
		toolbar.Size().Height - header.Size().Height - status.Size().Height
}

// Muestra el mensaje de la barra de estado.
func setStatus(cont *fyne.Container, connected, start, tracking bool) {
	msg := localize(msgStatus)

	if connected {
		msg += " Connected to " + pref.String("host")
		antStatus = Connected
	} else {
		msg += " Not connected"
		antStatus = Disconnect
	}

	if start {
		msg += " | Started"
		antStatus = Started
		MainWidgets.startBtn.Text = localize(msgStop)
	} else {
		msg += " | Stoped"
		MainWidgets.startBtn.Text = localize(msgStart)
	}

	if tracking {
		msg += " | Tracking"
		antStatus = Tracking
		MainWidgets.trackBtn.Text = localize(msgTracking)
	} else {
		msg += " | No tracking"
		MainWidgets.trackBtn.Text = localize(msgTrack)
	}

	cont.RemoveAll()
	cont.Add(container.NewVBox(widget.NewSeparator(), container.NewHBox(widget.NewLabel(msg))))
	cont.Refresh()
	MainWidgets.startBtn.Refresh()
	MainWidgets.trackBtn.Refresh()
}

// makePopMenu crea el menú principal de la aplicación.
func makePopMenu(w fyne.Window) *fyne.Menu {
	conf := fyne.NewMenuItem(localize(msgConfiguration), func() {
		content.RemoveAll()
		content.Add(makeConfig(w))
		loadPreferences()
		configVisible = true
	})
	conf.Icon = theme.SettingsIcon()

	ape := fyne.NewMenuItem(localize(msgAppearance), func() {
		wSet := mySettings.set.LoadAppearanceScreen(w)
		w.SetFixedSize(true)
		w.Resize(wSet.MinSize())
		content.RemoveAll()
		content.Add(wSet)
		content.Refresh()
		mySettings.isVisible = true
	})
	ape.Icon = resource.AppearanceIcon()

	control := fyne.NewMenuItem(localize(msgControl), func() {
		content.RemoveAll()
		content.Add(makeControl(w))
		content.Refresh()
		status.Refresh()
	})
	control.Icon = resource.ControlIcon()

	log := fyne.NewMenuItem(localize(msgLog), func() {
		content.RemoveAll()
		content.Add(makeLog(w))
		content.Refresh()
		status.Refresh()
	})
	log.Icon = resource.LogIcon()

	m := fyne.NewMenu(localize(msgSettings), conf, ape, control, log)

	return m
}

// makeTitle crea el título de la pantalla principal.
func makeTitle() *widget.Label {
	l := widget.NewLabel("GoHAMSatTracker")
	l.TextStyle.Bold = true
	l.Alignment = fyne.TextAlignCenter
	return l
}

// makeConfig crea la pantalla de configuración.
func makeConfig(win fyne.Window) *fyne.Container {
	var w *widget.Entry

	// Título
	title := widget.NewLabel(localize(msgConfiguration))
	title.TextStyle.Bold = true
	title.Alignment = fyne.TextAlignCenter

	// Lenguaje
	langs := []string{"English", "Spanish"}
	sel := widget.NewSelect(langs, langSelected)

	if sel.PlaceHolder = getBind("language"); sel.PlaceHolder == "" {
		sel.PlaceHolder = "English"
	}

	language := widget.NewAccordion(widget.NewAccordionItem(localize(msgLanguage), sel))

	// Amsat
	w = widget.NewEntryWithData(binds.amsat)
	w.Validator = validator.Config.URL
	wamsat := container.NewVBox(widget.NewLabel(localize(msgAmsat)), w)

	// Directorio de datos
	wl := widget.NewLabelWithData(binds.directory)
	wdir := container.NewVBox(widget.NewLabel(localize(msgDirectory)), wl)

	// Host o IP
	w = widget.NewEntryWithData(binds.host)
	w.Validator = validator.Config.IPOrHost
	whost := container.NewVBox(widget.NewLabel(localize(msgHost)), w)

	// Intervalo
	w = widget.NewEntryWithData(binds.interval)
	w.Validator = validator.Config.Uint16
	winterval := container.NewVBox(widget.NewLabel(localize(msgInterval)), w)

	// Batería
	w = widget.NewEntryWithData(binds.battWarn)
	w.Validator = validator.Config.Float
	wbatt := container.NewVBox(widget.NewLabel(localize(msgBattery)), w)

	formProgram := container.NewVBox(wamsat, wdir, whost, winterval, wbatt)
	program := widget.NewAccordion(widget.NewAccordionItem(localize(msgProgram),
		formProgram))

	// Latitud del observador
	w = widget.NewEntryWithData(binds.obsLatitude)
	w.Validator = validator.Config.Latitude
	wlat := widget.NewFormItem(localize(msgLatitude), w)

	// Longitud del observador
	w = widget.NewEntryWithData(binds.obsLongitude)
	w.Validator = validator.Config.Longitude
	wlong := widget.NewFormItem(localize(msgLongitude), w)

	// Elevación del observador
	w = widget.NewEntryWithData(binds.obsElevation)
	w.Validator = validator.Config.Int
	welev := widget.NewFormItem(localize(msgObsElevation), w)

	formObserver := widget.NewForm(wlat, wlong, welev)
	observer := widget.NewAccordion(widget.NewAccordionItem(localize(msgObserver),
		formObserver))

	// Backlash de los motores
	w = widget.NewEntryWithData(binds.backLsEl)
	w.Validator = validator.Config.Uint16
	wBsEl := widget.NewFormItem(localize(msgBlsElevation), w)

	w = widget.NewEntryWithData(binds.backLsAz)
	w.Validator = validator.Config.Uint16
	wBsAz := widget.NewFormItem(localize(msgBlsAzimuth), w)

	w = widget.NewEntryWithData(binds.backLsRo)
	w.Validator = validator.Config.Uint16
	wBsRo := widget.NewFormItem(localize(msgBlsRotation), w)

	formBackslash := widget.NewForm(wBsEl, wBsAz, wBsRo)
	backlash := widget.NewAccordion(widget.NewAccordionItem(localize(msgBacklash),
		formBackslash))

	// Selector con el motor a configurar
	motor := widget.NewSelect([]string{localize(msgMElevation),
		localize(msgMAzimuth), localize(msgMRotation)}, func(motor string) {
		selectedMotor = motor
		content.RemoveAll()
		content.Add(makeConfig(win))
		content.Refresh()

	})
	mot := container.NewHBox(widget.NewLabel(localize(msgMotor)), motor)
	motor.Selected = selectedMotor
	motor.Refresh()

	type MotorValue int
	const (
		Min MotorValue = iota
		Max
		Steps
		Gears
		Speed
	)

	motorSel := func(value MotorValue) *widget.Entry {
		switch selectedMotor {
		case localize(msgMElevation):
			if value == Min {
				return widget.NewEntryWithData(binds.minRangeEl)
			}
			if value == Max {
				return widget.NewEntryWithData(binds.maxRangeEl)
			}
			if value == Steps {
				return widget.NewEntryWithData(binds.stepsRevEl)
			}
			if value == Gears {
				return widget.NewEntryWithData(binds.gearsMulEl)
			}
			if value == Speed {
				return widget.NewEntryWithData(binds.speedEl)
			}

		case localize(msgMAzimuth):
			if value == Min {
				return widget.NewEntryWithData(binds.minRangeAz)
			}
			if value == Max {
				return widget.NewEntryWithData(binds.maxRangeAz)
			}
			if value == Steps {
				return widget.NewEntryWithData(binds.stepsRevAz)
			}
			if value == Gears {
				return widget.NewEntryWithData(binds.gearsMulAz)
			}
			if value == Speed {
				return widget.NewEntryWithData(binds.speedAz)
			}

		case localize(msgMRotation):
			if value == Min {
				return widget.NewEntryWithData(binds.minRangeRo)
			}
			if value == Max {
				return widget.NewEntryWithData(binds.maxRangeRo)
			}
			if value == Steps {
				return widget.NewEntryWithData(binds.stepsRevRo)
			}
			if value == Gears {
				return widget.NewEntryWithData(binds.gearsMulRo)
			}
			if value == Speed {
				return widget.NewEntryWithData(binds.speedRo)
			}
		}
		return nil // Causa error. selectedMotor sin definir.
	}

	// Rango mínimo
	w = motorSel(Min)
	w.Validator = validator.Config.Degrees
	wminRange := widget.NewFormItem(localize(msgMinRange), w)

	// Rango máximo
	w = motorSel(Max)
	w.Validator = validator.Config.Degrees
	wmaxRange := widget.NewFormItem(localize(msgMaxRange), w)

	// Pasos
	w = motorSel(Steps)
	w.Validator = validator.Config.Uint16
	wsteps := widget.NewFormItem(localize(msgSteps), w)

	// Multiplicador de ratio de los engranajes
	w = motorSel(Gears)
	w.Validator = validator.Config.Float
	wgears := widget.NewFormItem(localize(msgGears), w)

	// Velocidad del motor
	w = motorSel(Speed)
	validator.SetMaxRpm(300)
	w.Validator = validator.Config.Rpm
	wspeed := widget.NewFormItem(localize(msgSpeed), w)

	formMotor := widget.NewForm(wminRange, wmaxRange, wsteps, wgears, wspeed)

	// Selector de encendido/apagado del motor
	enaSel := widget.NewSelect([]string{localize(msgOn), localize(msgOff)}, motorEnableTapped)
	// Indica si el pin enable está invertido
	enableInv := widget.NewCheck(localize(msgEnableInv), motorEnInvTapped)

	ena := container.NewHBox(widget.NewLabel(localize(msgEnable)), enaSel, enableInv)

	switch selectedMotor {
	case localize(msgMElevation):
		if getBBind("enableEl") {
			enaSel.Selected = localize(msgOn)
		} else {
			enaSel.Selected = localize(msgOff)
		}

		if getBBind("enableInvEl") {
			enableInv.SetChecked(true)
		} else {
			enableInv.SetChecked(false)
		}
	case localize(msgMAzimuth):
		if getBBind("enableAz") {
			enaSel.Selected = localize(msgOn)
		} else {
			enaSel.Selected = localize(msgOff)
		}

		if getBBind("enableInvAz") {
			enableInv.SetChecked(true)
		} else {
			enableInv.SetChecked(false)
		}
	case localize(msgMRotation):
		if getBBind("enableRo") {
			enaSel.Selected = localize(msgOn)
		} else {
			enaSel.Selected = localize(msgOff)
		}

		if getBBind("enableInvRo") {
			enableInv.SetChecked(true)
		} else {
			enableInv.SetChecked(false)
		}
	}
	enaSel.Refresh()

	motorCont := container.NewVBox(mot, formMotor, ena)
	cardMotor := widget.NewCard("", "", motorCont)

	// Botón de cierre
	btnClose := widget.NewButton(localize(msgClose), closeTapped)
	btnClose.Importance = widget.HighImportance

	// Botón de descarga de datos TLE
	btnDownload := widget.NewButton(localize(msgDownload), downloadTapped)
	btnDownload.Importance = widget.HighImportance

	// Botón de prueba de los límites de los motores
	btnMtrLim := widget.NewButton(localize(msgMLimits), motorsLimitsTapped)
	btnMtrLim.Importance = widget.HighImportance

	// Botón de prueba de pasada a 90 grados
	btn90Pass := widget.NewButton(localize(msg90Pass), pass90Tapped)
	btn90Pass.Importance = widget.HighImportance

	// Botón de dexconexión de motores
	btnDisMotor := widget.NewButton(localize(msgDisMotor), disMotorTapped)
	btnDisMotor.Importance = widget.HighImportance

	if antStatus != Started {
		btnMtrLim.Disable()
		btn90Pass.Disable()
		btnDisMotor.Disable()
	}

	vCont := container.NewVBox(language, program, observer, backlash, cardMotor,
		btnClose, btnDownload, btnMtrLim, btn90Pass, btnDisMotor)
	configWinSize := fyne.NewSize(win.Canvas().Size().Width, ContentHeight())

	vScroll := container.NewVScroll(vCont)
	vScroll.SetMinSize(configWinSize)
	vCont.Resize(configWinSize)

	return container.NewMax(vScroll)
}

// makeLog muestra la pantalla de logs del programa.
func makeLog(win fyne.Window) *fyne.Container {
	pref := fyne.CurrentApp().Preferences()
	logText := core.ReadFile(pref.String("DATA_DIR") + "log.txt")

	text := ""
	if logText != nil {
		text = string(*logText)
	}

	// Texto del log
	textBox := widget.NewRichTextWithText(text)

	// Botón de cierre
	btnClose := widget.NewButton(localize(msgClose), closeLogTapped)
	btnClose.Importance = widget.HighImportance

	// Se cambia el tamaño fijo del cuadro de texto
	newSize := fyne.NewSize(win.Canvas().Size().Width, ContentHeight()-
		btnClose.Size().Height)
	vScroll := container.NewVScroll(container.NewVBox(textBox, btnClose))
	vScroll.SetMinSize(newSize)

	return container.NewMax(vScroll)
}

// makeBindings crea los vinculos entre los campos de configuración y las preferencias
// de la aplicación. Quedandose guardados cada vez que se modifican.
func makeBindings() *prefBindings {

	p := &prefBindings{
		amsat:        binding.NewString(),
		directory:    binding.NewString(),
		host:         binding.NewString(),
		interval:     binding.NewString(),
		obsLatitude:  binding.NewString(),
		obsLongitude: binding.NewString(),
		obsElevation: binding.NewString(),
		minRangeEl:   binding.NewString(),
		maxRangeEl:   binding.NewString(),
		stepsRevEl:   binding.NewString(),
		gearsMulEl:   binding.NewString(),
		speedEl:      binding.NewString(),
		enableEl:     binding.NewBool(),
		enableInvEl:  binding.NewBool(),
		minRangeAz:   binding.NewString(),
		maxRangeAz:   binding.NewString(),
		stepsRevAz:   binding.NewString(),
		gearsMulAz:   binding.NewString(),
		speedAz:      binding.NewString(),
		enableAz:     binding.NewBool(),
		enableInvAz:  binding.NewBool(),
		minRangeRo:   binding.NewString(),
		maxRangeRo:   binding.NewString(),
		stepsRevRo:   binding.NewString(),
		gearsMulRo:   binding.NewString(),
		speedRo:      binding.NewString(),
		enableRo:     binding.NewBool(),
		enableInvRo:  binding.NewBool(),
		language:     binding.NewString(),
		backLsEl:     binding.NewString(),
		backLsAz:     binding.NewString(),
		backLsRo:     binding.NewString(),
		battWarn:     binding.NewString(),
	}

	return p
}

// loadPreferences carga las preferencias de la aplicación
func loadPreferences() {
	dev := fyne.CurrentDevice()

	binds.amsat.Set(pref.StringWithFallback("AMSAT_URL",
		"http://www.amsat.org/amsat/ftp/keps/current/nasa.all"))

	if dev.IsMobile() {
		binds.directory.Set(pref.StringWithFallback("DATA_DIR",
			"file:///data/data/"+fyne.CurrentApp().UniqueID()+"/"))
	} else {
		binds.directory.Set(pref.StringWithFallback("DATA_DIR", "file://./"))
	}
	binds.host.Set(pref.StringWithFallback("host", "127.0.0.1"))
	binds.interval.Set(pref.StringWithFallback("interval", "1300"))
	binds.obsLatitude.Set(pref.String("obsLatitude"))
	binds.obsLongitude.Set(pref.String("obsLongitude"))
	binds.obsElevation.Set(pref.String("obsElevation"))
	binds.minRangeEl.Set(pref.StringWithFallback("minRangeEl", "-50"))
	binds.maxRangeEl.Set(pref.StringWithFallback("maxRangeEl", "75"))
	binds.stepsRevEl.Set(pref.StringWithFallback("stepsRevEl", "800"))
	binds.gearsMulEl.Set(pref.StringWithFallback("gearsMulEl", "4.0"))
	binds.speedEl.Set(pref.StringWithFallback("speedEl", "3"))
	binds.minRangeAz.Set(pref.StringWithFallback("minRangeAz", "-360"))
	binds.maxRangeAz.Set(pref.StringWithFallback("maxRangeAz", "360"))
	binds.stepsRevAz.Set(pref.StringWithFallback("stepsRevAz", "3200"))
	binds.gearsMulAz.Set(pref.StringWithFallback("gearsMulAz", "2.5"))
	binds.speedAz.Set(pref.StringWithFallback("speedAz", "5"))
	binds.minRangeRo.Set(pref.StringWithFallback("minRangeRo", "-90"))
	binds.maxRangeRo.Set(pref.StringWithFallback("maxRangeRo", "90"))
	binds.stepsRevRo.Set(pref.StringWithFallback("stepsRevRo", "3200"))
	binds.gearsMulRo.Set(pref.StringWithFallback("gearsMulRo", "1.0"))
	binds.speedRo.Set(pref.StringWithFallback("speedRo", "3"))
	binds.backLsEl.Set(pref.StringWithFallback("backLsEl", "3"))
	binds.backLsAz.Set(pref.StringWithFallback("backLsAz", "5"))
	binds.backLsRo.Set(pref.StringWithFallback("backLsRo", "1"))
	binds.battWarn.Set(pref.StringWithFallback("battWarn", "20.4"))
	binds.enableAz.Set(pref.Bool("enableAz"))
	binds.enableInvAz.Set(pref.Bool("enableInvAz"))
	binds.enableEl.Set(pref.Bool("enableEl"))
	binds.enableInvEl.Set(pref.Bool("enableInvEl"))
	binds.enableRo.Set(pref.Bool("enableRo"))
	binds.enableInvRo.Set(pref.Bool("enableInvRo"))
	binds.language.Set(pref.String("language"))
}

// savePreferences guarda las preferencias de la aplicación.
func savePreferences() {
	dev := fyne.CurrentDevice()

	amsat, _ := binds.amsat.Get()
	pref.SetString("AMSAT_URL", amsat)
	if dev.IsMobile() {
		pref.SetString("DATA_DIR",
			"file:///data/data/"+fyne.CurrentApp().UniqueID()+"/")
	} else {
		pref.SetString("DATA_DIR", "file://./")
	}

	if pref.String("TLEFilename") == "" {
		pref.SetString("TLEFilename", "nasa.all")
	}

	pref.SetString("host", getBind("host"))
	pref.SetString("interval", getBind("interval"))
	pref.SetString("obsLatitude", getBind("obsLatitude"))
	pref.SetString("obsLongitude", getBind("obsLongitude"))
	pref.SetString("obsElevation", getBind("obsElevation"))
	pref.SetString("minRangeEl", getBind("minRangeEl"))
	pref.SetString("maxRangeEl", getBind("maxRangeEl"))
	pref.SetString("gearsMulEl", getBind("gearsMulEl"))
	pref.SetString("speedEl", getBind("speedEl"))
	pref.SetString("stepsRevEl", getBind("stepsRevEl"))
	pref.SetString("minRangeAz", getBind("minRangeAz"))
	pref.SetString("maxRangeAz", getBind("maxRangeAz"))
	pref.SetString("stepsRevAz", getBind("stepsRevAz"))
	pref.SetString("gearsMulAz", getBind("gearsMulAz"))
	pref.SetString("speedAz", getBind("speedAz"))
	pref.SetString("minRangeRo", getBind("minRangeRo"))
	pref.SetString("maxRangeRo", getBind("maxRangeRo"))
	pref.SetString("stepsRevRo", getBind("stepsRevRo"))
	pref.SetString("gearsMulRo", getBind("gearsMulRo"))
	pref.SetString("speedRo", getBind("speedRo"))
	pref.SetString("language", getBind("language"))
	pref.SetString("backLsEl", getBind("backLsEl"))
	pref.SetString("backLsAz", getBind("backLsAz"))
	pref.SetString("backLsRo", getBind("backLsRo"))
	pref.SetString("battWarn", getBind("battWarn"))
	pref.SetBool("enableAz", getBBind("enableAz"))
	pref.SetBool("enableInvAz", getBBind("enableInvAz"))
	pref.SetBool("enableEl", getBBind("enableEl"))
	pref.SetBool("enableInvEl", getBBind("enableInvEl"))
	pref.SetBool("enableRo", getBBind("enableRo"))
	pref.SetBool("enableInvRo", getBBind("enableInvRo"))
}

func prefsToStruct() *core.PrefsCBOR {
	data := core.PrefsCBOR{}

	tmpUint, _ := strconv.ParseUint(pref.String("interval"), 10, 16)
	data.Interv = uint16(tmpUint)

	tmpFloat, _ := strconv.ParseFloat(pref.String("obsLatitude"), 32)
	data.ObsLatitude = float32(tmpFloat * (math.Pi / 180.0)) // Conversión a radianes.

	tmpFloat, _ = strconv.ParseFloat(pref.String("obsLongitude"), 32)
	data.ObsLongitude = float32(tmpFloat * (math.Pi / 180.0))

	elev, _ := strconv.ParseUint(pref.String("obsElevation"), 10, 32)
	data.ObsElevation = float32(elev / 1000.0) // Conversión a kilómetros.

	tmpInt, _ := strconv.ParseInt(pref.String("minRangeEl"), 10, 32)
	data.MinRangeEl = int(tmpInt)

	tmpInt, _ = strconv.ParseInt(pref.String("maxRangeEl"), 10, 32)
	data.MaxRangeEl = int(tmpInt)

	tmpUint, _ = strconv.ParseUint(pref.String("stepsRevEl"), 10, 16)
	data.StepsRevEl = uint16(tmpUint)

	tmpFloat, _ = strconv.ParseFloat(pref.String("gearsMulEl"), 32)
	data.GearsMulEl = float32(tmpFloat)

	tmpUint, _ = strconv.ParseUint(pref.String("speedEl"), 10, 16)
	data.SpeedEl = uint16(tmpUint)

	tmpInt, _ = strconv.ParseInt(pref.String("minRangeAz"), 10, 32)
	data.MinRangeAz = int(tmpInt)

	tmpInt, _ = strconv.ParseInt(pref.String("maxRangeAz"), 10, 32)
	data.MaxRangeAz = int(tmpInt)

	tmpUint, _ = strconv.ParseUint(pref.String("stepsRevAz"), 10, 16)
	data.StepsRevAz = uint16(tmpUint)

	tmpFloat, _ = strconv.ParseFloat(pref.String("gearsMulAz"), 32)
	data.GearsMulAz = float32(tmpFloat)

	tmpUint, _ = strconv.ParseUint(pref.String("speedAz"), 10, 16)
	data.SpeedAz = uint16(tmpUint)

	tmpInt, _ = strconv.ParseInt(pref.String("minRangeRo"), 10, 32)
	data.MinRangeRo = int(tmpInt)

	tmpInt, _ = strconv.ParseInt(pref.String("maxRangeRo"), 10, 32)
	data.MaxRangeRo = int(tmpInt)

	tmpUint, _ = strconv.ParseUint(pref.String("stepsRevRo"), 10, 16)
	data.StepsRevRo = uint16(tmpUint)

	tmpFloat, _ = strconv.ParseFloat(pref.String("gearsMulRo"), 32)
	data.GearsMulRo = float32(tmpFloat)

	tmpUint, _ = strconv.ParseUint(pref.String("speedRo"), 10, 16)
	data.SpeedRo = uint16(tmpUint)

	tmpUint, _ = strconv.ParseUint(pref.String("backLsEl"), 10, 16)
	data.BackLsEl = uint16(tmpUint)

	tmpUint, _ = strconv.ParseUint(pref.String("backLsAz"), 10, 16)
	data.BackLsAz = uint16(tmpUint)

	tmpUint, _ = strconv.ParseUint(pref.String("backLsRo"), 10, 16)
	data.BackLsRo = uint16(tmpUint)

	data.EnableAz = pref.Bool("enableAz")
	data.EnableEl = pref.Bool("enableEl")
	data.EnableRo = pref.Bool("enableRo")
	data.EnableInvAz = pref.Bool("enableInvAz")
	data.EnableInvEl = pref.Bool("enableInvEl")
	data.EnableInvRo = pref.Bool("enableInvRo")
	data.TLEline1 = ""
	data.TLEline2 = ""

	return &data
}

// makeControl muestra la pantalla para controlar los motores de la antena.
func makeControl(win fyne.Window) *fyne.Container {

	// Azimuth
	lAzimuth := widget.NewLabel(localize(msgMAzimuth))
	lAzimuth.TextStyle.Bold = true
	sepAzimuth := widget.NewSeparator()
	btnRight := widget.NewButton(localize(msgRight), rightTapped)
	btnRight.Importance = widget.HighImportance
	btnRight.Icon = resource.ArrowRightIcon()

	btnLeft := widget.NewButton(localize(msgLeft), leftTapped)
	btnLeft.Importance = widget.HighImportance
	btnLeft.Icon = resource.ArrowLeftIcon()

	// Azimuth slider
	aziMin := 1
	sdAzimuth := widget.NewSlider(float64(aziMin), strToFloat(getBind("stepsRevAz"))*
		strToFloat(getBind("gearsMulAz")))
	lblAzimuth := widget.NewLabel(localize(msgStepSd) + strconv.Itoa(aziMin))
	controlStepsAz = aziMin
	sdAzimuth.OnChanged = func(value float64) {
		lblAzimuth.SetText(localize(msgStepSd) + fmt.Sprintf("%3.f", value))
		controlStepsAz = int(value)
	}
	sdAzimuth.Step = float64(aziMin)

	hAzimuth := container.New(layout.NewGridLayout(2),
		container.NewPadded(btnLeft), container.NewPadded(btnRight),
		container.NewPadded(sdAzimuth), container.NewPadded(lblAzimuth))

	vAzimuth := container.NewVBox(lAzimuth, sepAzimuth, hAzimuth)

	// Elevation
	lElev := widget.NewLabel(localize(msgMElevation))
	lElev.TextStyle.Bold = true
	sepElev := widget.NewSeparator()

	btnUp := widget.NewButton(localize(msgUp), upTapped)
	btnUp.Importance = widget.HighImportance
	btnUp.Icon = resource.ArrowUpIcon()

	btnDown := widget.NewButton(localize(msgDown), downTapped)
	btnDown.Importance = widget.HighImportance
	btnDown.Icon = resource.ArrowDownIcon()

	// Elevation slider
	eleMin := 1
	sdElev := widget.NewSlider(float64(eleMin), strToFloat(getBind("stepsRevEl"))*
		strToFloat(getBind("gearsMulEl")))
	lblElev := widget.NewLabel(localize(msgStepSd) + strconv.Itoa(eleMin))
	controlStepsEl = eleMin
	sdElev.OnChanged = func(value float64) {
		lblElev.SetText(localize(msgStepSd) + fmt.Sprintf("%3.f", value))
		controlStepsEl = int(value)
	}
	sdElev.Step = float64(eleMin)

	vArrows := container.New(layout.NewGridLayout(2),
		container.NewPadded(btnUp), container.NewPadded(btnDown),
		container.NewPadded(sdElev), container.NewPadded(lblElev))

	vElev := container.NewVBox(lElev, sepElev, vArrows)

	// Rotation
	lRot := widget.NewLabel(localize(msgMRotation))
	lRot.TextStyle.Bold = true
	sepRot := widget.NewSeparator()

	btnRotLeft := widget.NewButton(localize(msgLeft), rotLeftTapped)
	btnRotLeft.Importance = widget.HighImportance
	btnRotLeft.Icon = resource.UndoIcon()

	btnRotRight := widget.NewButton(localize(msgRight), rotRightTapped)
	btnRotRight.Importance = widget.HighImportance
	btnRotRight.Icon = resource.RedoIcon()

	// Rotation slider
	rotMin := 1
	sdRot := widget.NewSlider(float64(rotMin), strToFloat(getBind("stepsRevRo"))*
		strToFloat(getBind("gearsMulRo")))
	lblRot := widget.NewLabel(localize(msgStepSd) + strconv.Itoa(rotMin))
	controlStepsRo = rotMin
	sdRot.OnChanged = func(value float64) {
		lblRot.SetText(localize(msgStepSd) + fmt.Sprintf("%3.f", value))
		controlStepsRo = int(value)
	}
	sdRot.Step = float64(rotMin)

	hRot := container.New(layout.NewGridLayout(2),
		container.NewPadded(btnRotLeft), container.NewPadded(btnRotRight),
		container.NewPadded(sdRot), container.NewPadded(lblRot))

	vRot := container.NewVBox(lRot, sepRot, hRot)

	// Botón de stop
	btnStop := widget.NewButton(localize(msgStop), stopTapped)
	btnStop.Importance = widget.HighImportance

	// Botón de posición inicial
	btnInitPos := widget.NewButton(localize(msgInitPos), initPosTapped)
	btnInitPos.Importance = widget.HighImportance

	// Botón de cierre
	btnClose := widget.NewButton(localize(msgClose), closeTapped)
	btnClose.Importance = widget.HighImportance

	newSize := fyne.NewSize(win.Canvas().Size().Width, ContentHeight()-
		btnClose.Size().Height*3)
	vScroll := container.NewVScroll(container.NewVBox(vAzimuth, vElev, vRot,
		layout.NewSpacer(), btnStop, btnInitPos, btnClose))
	vScroll.SetMinSize(newSize)

	if antStatus == Tracking {
		btnLeft.Disable()
		btnRight.Disable()
		btnUp.Disable()
		btnDown.Disable()
		btnInitPos.Disable()
	} else if antStatus != Started {
		btnLeft.Disable()
		btnRight.Disable()
		btnUp.Disable()
		btnDown.Disable()
		btnRotLeft.Disable()
		btnRotRight.Disable()
		btnStop.Disable()
		btnInitPos.Disable()
	}

	return container.NewMax(vScroll)
}

// makeMain crea la pantalla principal de la aplicación.
func makeMain() *fyne.Container {
	empty := "__________"
	tleFile := pref.String("DATA_DIR") + pref.String("TLEFilename")

	// Listado de satélites
	cachedSatNames = core.GetAllSat(tleFile)
	opts := []string{}
	sel := NewSearchSelect(opts)

	if cachedSatNames != nil {
		sel.SetOptions(cachedSatNames)
	}

	sel.PlaceHolder = localize(msgSelect)
	sel.OnChanged = sattelliteSelected

	if selectedSat != "" {
		sel.Text = selectedSat
	}

	MainWidgets = mainWidgets{latitude: widget.NewLabel(empty),
		longitude: widget.NewLabel(empty), distance: widget.NewLabel(empty),
		period: widget.NewLabel(empty), azimuth: widget.NewLabel(empty),
		elevation: widget.NewLabel(empty), altitude: widget.NewLabel(empty),
		speed: widget.NewLabel(empty), locTime: widget.NewLabel(empty),
		utcTime: widget.NewLabel(empty), startBtn: widget.NewButton(empty, nil),
		trackBtn: widget.NewButton(empty, nil), battery: widget.NewLabel(empty)}

	// Latitud
	lat := widget.NewFormItem(localize(msgLatitude), MainWidgets.latitude)
	// Longitud
	long := widget.NewFormItem(localize(msgLongitude), MainWidgets.longitude)
	// Distancia
	dis := widget.NewFormItem(localize(msgDistance), MainWidgets.distance)
	// Período
	per := widget.NewFormItem(localize(msgPeriod), MainWidgets.period)
	// Azimut
	azi := widget.NewFormItem(localize(msgAzimuth), MainWidgets.azimuth)
	// Elevación
	ele := widget.NewFormItem(localize(msgElevation), MainWidgets.elevation)
	// Altitud
	alt := widget.NewFormItem(localize(msgAltitude), MainWidgets.altitude)
	// Velocidad
	spe := widget.NewFormItem(localize(msgSpeed), MainWidgets.speed)

	form := widget.NewCard("", "", widget.NewForm(lat, long, dis, per, azi, ele, alt, spe))

	// Hora UTC
	lTime := widget.NewLabel(localize(msgTime))
	// Hora local
	lLocal := widget.NewLabel("(" + localize(msgLocal) + ")")
	timeCont := container.NewHBox(lTime, MainWidgets.utcTime,
		MainWidgets.locTime, lLocal)

	// Botón de comienzo de cálculo.
	if antStatus == Started {
		MainWidgets.startBtn.Text = localize(msgStop)
	} else {
		MainWidgets.startBtn.Text = localize(msgStart)
	}

	MainWidgets.startBtn.OnTapped = startTapped
	MainWidgets.startBtn.Importance = widget.HighImportance
	btnCont := container.NewGridWithColumns(2, container.NewPadded(MainWidgets.startBtn))

	// Botón de comienzo de seguimiento con la antena.
	if antStatus == Tracking {
		MainWidgets.trackBtn.Text = localize(msgTracking)
	} else {
		MainWidgets.trackBtn.Text = localize(msgTrack)
	}

	MainWidgets.trackBtn.OnTapped = trackTapped
	MainWidgets.trackBtn.Importance = widget.HighImportance
	btnCont.Add(container.NewPadded(MainWidgets.trackBtn))

	sep := widget.NewSeparator()
	batt := container.NewHBox(widget.NewLabel(localize(msgBatteryVolt)),
		MainWidgets.battery)

	newsize := fyne.NewSize(btnCont.MinSize().Width, ContentHeight())

	vScroll := container.NewVScroll(container.NewVBox(sel, form, timeCont, btnCont, sep, batt))
	vScroll.SetMinSize(newsize)

	return container.NewMax(vScroll)
}

// getBind devuelve el valor de la variable asociada a un elemento gráfico de la configuración.
func getBind(bind string) (value string) {

	fTable := map[string]func() (string, error){"host": binds.host.Get,
		"interval": binds.interval.Get, "obsLatitude": binds.obsLatitude.Get,
		"obsLongitude": binds.obsLongitude.Get, "obsElevation": binds.obsElevation.Get,
		"minRangeEl": binds.minRangeEl.Get, "maxRangeEl": binds.maxRangeEl.Get,
		"stepsRevEl": binds.stepsRevEl.Get, "gearsMulEl": binds.gearsMulEl.Get,
		"minRangeAz": binds.minRangeAz.Get, "maxRangeAz": binds.maxRangeAz.Get,
		"stepsRevAz": binds.stepsRevAz.Get, "gearsMulAz": binds.gearsMulAz.Get,
		"minRangeRo": binds.minRangeRo.Get, "maxRangeRo": binds.maxRangeRo.Get,
		"stepsRevRo": binds.stepsRevRo.Get, "gearsMulRo": binds.gearsMulRo.Get,
		"speedEl": binds.speedEl.Get, "speedAz": binds.speedAz.Get,
		"speedRo": binds.speedRo.Get, "backLsEl": binds.backLsEl.Get,
		"backLsAz": binds.backLsAz.Get, "backLsRo": binds.backLsRo.Get,
		"battWarn": binds.battWarn.Get, "language": binds.language.Get,
	}

	if function, ok := fTable[bind]; ok {
		val, _ := function()
		return val
	}

	return ""
}

// getBBind devuelve el valor boleano de la variable asociada a un elemento gráfico de la configuración.
func getBBind(bind string) (value bool) {

	fTable := map[string]func() (bool, error){"enableAz": binds.enableAz.Get,
		"enableEl": binds.enableEl.Get, "enableRo": binds.enableRo.Get,
		"enableInvAz": binds.enableInvAz.Get, "enableInvEl": binds.enableInvEl.Get,
		"enableInvRo": binds.enableInvRo.Get,
	}

	if function, ok := fTable[bind]; ok {
		val, _ := function()
		return val
	}

	return false
}

// localize devuelve el mensaje traducido sin inclir el error para evitar el manejo del error
// de forma recurrente.
func localize(msg *i18n.Message) (value string) {
	lang, _ := binds.language.Get()

	switch lang {
	case "English":
		localizer = i18n.NewLocalizer(bundle, language.English.String())
	case "Spanish":
		localizer = i18n.NewLocalizer(bundle, language.Spanish.String())
	}

	if val, error := localizer.LocalizeMessage(msg); error == nil {
		return val
	} else {
		panic("Message not found")
	}

}

// StrToInt convierte una cadena a flotante sin pasar errores.
func strToFloat(s string) float64 {
	num, error := strconv.ParseFloat(s, 64)

	if error == nil {
		return num
	}

	return 0
}
