/*
File: stepper_test.go
Project: GoHAMSatTracker
Created Date: Tuesday, October 4th 2022, 2:39:42 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

package board

import (
	"fmt"
	"testing"
	"time"

	mock_board "GoHAMSatTracker/internal/pkg/board/mocks"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func mockDriverOutChk(t *testing.T) (driver *A4988) {
	ctrl := gomock.NewController(t)
	rpioMock := func() error {
		fmt.Println("Mocking rpio.Open()")
		return nil
	}

	pins := make(map[string]*mock_board.MockPinI)
	pins["ena"] = mock_board.NewMockPinI(ctrl)
	pins["slp"] = mock_board.NewMockPinI(ctrl)
	pins["pul"] = mock_board.NewMockPinI(ctrl)
	pins["dir"] = mock_board.NewMockPinI(ctrl)
	pins["ms1"] = mock_board.NewMockPinI(ctrl)
	pins["ms2"] = mock_board.NewMockPinI(ctrl)
	pins["ms3"] = mock_board.NewMockPinI(ctrl)

	pins["ena"].EXPECT().Output().Times(1)
	pins["slp"].EXPECT().Output().Times(1)
	pins["pul"].EXPECT().Output().Times(1)
	pins["dir"].EXPECT().Output().Times(1)
	pins["ms1"].EXPECT().Output().Times(1)
	pins["ms2"].EXPECT().Output().Times(1)
	pins["ms3"].EXPECT().Output().Times(1)

	gomock.InOrder(
		pins["ena"].EXPECT().High().Times(1),
		pins["dir"].EXPECT().High().Times(1),
		pins["pul"].EXPECT().Low().Times(1),
		pins["slp"].EXPECT().High().Times(1),
	)

	driver = newDriverA4988(pins["ena"], pins["slp"], pins["pul"],
		pins["dir"], pins["ms1"], pins["ms2"], pins["ms3"],
		none, 400, false, rpioMock)

	return
}

func mockDriver(t *testing.T, pinsInOrder bool) (driver *A4988, pins map[string]*mock_board.MockPinI) {
	ctrl := gomock.NewController(t)
	rpioMock := func() error {
		fmt.Println("Mocking rpio.Open()")
		return nil
	}

	pins = make(map[string]*mock_board.MockPinI)
	pins["ena"] = mock_board.NewMockPinI(ctrl)
	pins["slp"] = mock_board.NewMockPinI(ctrl)
	pins["pul"] = mock_board.NewMockPinI(ctrl)
	pins["dir"] = mock_board.NewMockPinI(ctrl)
	pins["ms1"] = mock_board.NewMockPinI(ctrl)
	pins["ms2"] = mock_board.NewMockPinI(ctrl)
	pins["ms3"] = mock_board.NewMockPinI(ctrl)

	pins["ena"].EXPECT().Output().AnyTimes()
	pins["slp"].EXPECT().Output().AnyTimes()
	pins["pul"].EXPECT().Output().AnyTimes()
	pins["dir"].EXPECT().Output().AnyTimes()
	pins["ms1"].EXPECT().Output().AnyTimes()
	pins["ms2"].EXPECT().Output().AnyTimes()
	pins["ms3"].EXPECT().Output().AnyTimes()

	if pinsInOrder {
		gomock.InOrder(
			pins["ena"].EXPECT().High().Times(1),
			pins["dir"].EXPECT().High().Times(1),
			pins["pul"].EXPECT().Low().Times(1),
			pins["slp"].EXPECT().High().Times(1),
		)
	} else {
		pins["ena"].EXPECT().High().Times(1)
		//pins["ena"].EXPECT().Low().Times(1)
		pins["dir"].EXPECT().High().AnyTimes()
		pins["dir"].EXPECT().Low().AnyTimes()
		pins["pul"].EXPECT().High().AnyTimes()
		pins["pul"].EXPECT().Low().AnyTimes()
		pins["slp"].EXPECT().High().Times(1)
		//pins["slp"].EXPECT().Low().Times(1)
	}

	driver = newDriverA4988(pins["ena"], pins["slp"], pins["pul"],
		pins["dir"], pins["ms1"], pins["ms2"], pins["ms3"],
		none, 400, false, rpioMock)

	return
}

func stepCounter(t *testing.T, count int) (pulseHCounter,
	pulseLCounter int) {
	driver, pins := mockDriver(t, true)

	pins["dir"].EXPECT().High().Times(1)
	pins["pul"].EXPECT().High().AnyTimes().
		Do(func() {
			pulseHCounter++
		})

	pins["pul"].EXPECT().Low().AnyTimes().
		Do(func() {
			pulseLCounter++
		})

	driver.moveSteps(count)

	<-driver.done

	return
}

func revDegCounter(t *testing.T, count float32, isRevolutions bool) (pulseCounter int) {
	driver, pins := mockDriver(t, true)

	pins["dir"].EXPECT().High().Times(1)

	pins["pul"].EXPECT().High().AnyTimes().
		Do(func() {
			pulseCounter++
		})

	pins["pul"].EXPECT().Low().AnyTimes()

	if isRevolutions {
		driver.moveRevolutions(count)
	} else {
		driver.moveDegrees(count)
	}

	<-driver.done

	return
}

func pulsePeriod(t *testing.T) (period time.Duration, pulseHCounter int) {
	d, pins := mockDriver(t, true)

	const numPulses = 400 // una vuelta según conf. por defecto.
	times := [numPulses]time.Time{}

	pins["dir"].EXPECT().High().Times(1)

	pins["pul"].EXPECT().High().Times(numPulses).
		Do(func() {
			times[pulseHCounter] = time.Now()
			pulseHCounter++
		})

	pins["pul"].EXPECT().Low().AnyTimes()

	d.moveSteps(numPulses)

	<-d.done

	var totalTime time.Duration
	for i := 0; i < len(times)/2; i++ {
		diff := times[i+1].Sub(times[i])
		totalTime += diff
	}

	// devuelve la media.
	return totalTime / time.Duration(len(times)/2), pulseHCounter
}

func TestNewDriverDefaults(t *testing.T) {
	d := mockDriverOutChk(t)

	assert.EqualValues(t, d.configSteps, none)
	assert.EqualValues(t, d.stepsPerRev, 400)
	assert.EqualValues(t, d.enableInv, false)
	assert.EqualValues(t, d.speedRpm, 10)
	assert.EqualValues(t, d.maxSpeedRpm, 300)
	assert.EqualValues(t, d.pulseWidthUs, 5)
	assert.EqualValues(t, d.timeBPulsesUs, 5)
}

func TestA4988ConfigSteps(t *testing.T) {
	var expMs1, expMs2, expMs3 uint8

	for i := 0; i < 6; i++ {
		// para resetear el número de ejecuciónes de los pines.
		d, pins := mockDriver(t, true)

		switch i {
		case 0:
			d.configSteps = fullStep
			expMs1 = 0
			expMs2 = 0
			expMs3 = 0
		case 1:
			d.configSteps = halfStep
			expMs1 = 1
			expMs2 = 0
			expMs3 = 0
		case 2:
			d.configSteps = quarterStep
			expMs1 = 0
			expMs2 = 1
			expMs3 = 0
		case 3:
			d.configSteps = eighthStep
			expMs1 = 1
			expMs2 = 1
			expMs3 = 0
		case 4:
			d.configSteps = sixteenthStep
			expMs1 = 1
			expMs2 = 1
			expMs3 = 1
		case 5:
			d.configSteps = none

		}

		// se empieza con el motor parado, dirección hacia adelante,
		// pulse a 0 y sleep activado

		gomock.InOrder(
			pins["ena"].EXPECT().High().Times(1),
			pins["dir"].EXPECT().High().Times(1),
			pins["pul"].EXPECT().Low().Times(1),
			pins["slp"].EXPECT().High().Times(1),
		)

		if d.configSteps == none {
			pins["ms1"].EXPECT().High().MaxTimes(0)
			pins["ms2"].EXPECT().High().MaxTimes(0)
			pins["ms3"].EXPECT().High().MaxTimes(0)
			pins["ms1"].EXPECT().Low().MaxTimes(0)
			pins["ms2"].EXPECT().Low().MaxTimes(0)
			pins["ms3"].EXPECT().Low().MaxTimes(0)

		} else {

			if expMs1 == 1 {
				pins["ms1"].EXPECT().High().Times(1)
			} else {
				pins["ms1"].EXPECT().Low().Times(1)
			}

			if expMs2 == 1 {
				pins["ms2"].EXPECT().High().Times(1)
			} else {
				pins["ms2"].EXPECT().Low().Times(1)
			}

			if expMs3 == 1 {
				pins["ms3"].EXPECT().High().Times(1)
			} else {
				pins["ms3"].EXPECT().Low().Times(1)
			}
		}

		d.init()
	}
}

func TestEnableDisableMotor(t *testing.T) {
	var fun func()

	for i := 0; i < 2; i++ {
		d, pins := mockDriver(t, true)

		switch i {
		case 0: // enable motor
			fun = d.enableMotor
			gomock.InOrder(
				pins["ena"].EXPECT().Low().Times(1),
				pins["ena"].EXPECT().High().Times(1),
			)
		case 1: // disable motor
			fun = d.disableMotor
			gomock.InOrder(
				pins["ena"].EXPECT().High().Times(1),
				pins["ena"].EXPECT().Low().Times(1),
			)
		}

		fun()
		d.enableInv = true
		fun()
	}
}

func TestGetSpeed(t *testing.T) {
	d, _ := mockDriver(t, true)

	speed := d.getSpeed()
	assert.EqualValues(t, d.speedRpm, speed)
}

func TestSetMaxRpm(t *testing.T) {
	d, _ := mockDriver(t, true)
	d.setMaxRpm(200)
	assert.EqualValues(t, 200, d.maxSpeedRpm)

	max := d.getMaxSpeed()
	d.setMaxRpm(max)
	assert.EqualValues(t, 200, d.maxSpeedRpm)

	d.setMaxRpm(max + 1)
	assert.EqualValues(t, 200, d.maxSpeedRpm)

	d.setMaxRpm(max - 1)
	assert.EqualValues(t, (max - 1), d.maxSpeedRpm)

	d.setMaxRpm(0)
	assert.EqualValues(t, 0, d.maxSpeedRpm)
}

func TestSetSpeed(t *testing.T) {
	d, _ := mockDriver(t, true)
	max := d.getMaxSpeed()

	// sin limitación manual
	d.setMaxRpm(0)
	d.setSpeed(max + 1)
	assert.EqualValues(t, max, d.speedRpm)

	d.setSpeed(max)
	assert.EqualValues(t, max, d.speedRpm)

	d.setSpeed(max - 1)
	assert.EqualValues(t, (max - 1), d.speedRpm)

	// con limitación manual
	d.setMaxRpm(300)
	d.setSpeed(max)
	assert.EqualValues(t, 300, d.speedRpm)

	d.setSpeed(300)
	assert.EqualValues(t, 300, d.speedRpm)

	d.setSpeed(299)
	assert.EqualValues(t, 299, d.speedRpm)
}

func TestGetMaxSpeed(t *testing.T) {
	d, _ := mockDriver(t, true)
	mSpeed := d.getMaxSpeed()
	assert.EqualValues(t, 15000, mSpeed)
}

func TestGetSpeedDelay(t *testing.T) {
	d, _ := mockDriver(t, true)
	mDelay := d.getStepDelay()
	assert.EqualValues(t, time.Duration(15000*time.Microsecond), mDelay)
}

func TestMoveSteps(t *testing.T) {
	pulseH, pulseL := stepCounter(t, 500)
	assert.EqualValues(t, 500, pulseH)
	assert.EqualValues(t, 500, pulseL)
}

func TestPulseDuration(t *testing.T) {
	period, _ := pulsePeriod(t)
	//expectedPeriod := 15000 * time.Microsecond
	// Nuevo periodo incluyendo aceleración y desaceleración
	expectedPeriod := 21000 * time.Microsecond
	// Se comprueba esperando un error máximo del 5% a causa de los propios test.
	assert.InDelta(t, expectedPeriod, period, float64(expectedPeriod)*0.05)
}

func TestMoveDriversAsync(t *testing.T) {
	el, _ := mockDriver(t, false)
	az, _ := mockDriver(t, false)
	ro, _ := mockDriver(t, false)

	start := time.Now()
	el.moveSteps(400 * 100) // 100 vueltas.
	stop := time.Now()
	// tiempo aprox. de los 3 motores en ejecución secuencial.
	syncTime := stop.Sub(start) * 3

	start = time.Now()
	el.moveSteps(400 * 100)
	az.moveSteps(400 * 100)
	ro.moveSteps(400 * 100)
	stop = time.Now()
	// tiempo en ejecución asíncrona.
	asyncTime := stop.Sub(start)

	// sencilla comparación para ver si realmente se están ejecutando de forma
	// "paralela" las go rutinas.
	assert.Less(t, asyncTime, syncTime)
	oneMotor := syncTime / 3
	// los 3 motores se mueven a la vez (tardan los mismo que uno solo, con 50% de error)
	assert.InDelta(t, oneMotor, asyncTime, float64(oneMotor)*0.50)
}

func TestHaltDriver(t *testing.T) {
	d, _ := mockDriver(t, false)
	done := make(chan bool)

	// se manda la señal de parada
	go func() {
		msg := <-d.halt
		if assert.True(t, msg) {
			done <- true
		}
	}()

	time.Sleep(1 * time.Millisecond)
	d.haltMotor()
	<-done

	// recibe la señal de que el driver ha terminado pero con el valor false
	// indicando que no ha completado los pasos especificados
	go func() {
		msg := <-d.done
		if assert.False(t, msg) {
			done <- true
		}
	}()

	d.moveSteps(4000)
	time.Sleep(1 * time.Millisecond)
	d.haltMotor()
	<-done

	// recibe la señal de que el driver ha terminado con los pasos indicados
	go func() {
		msg := <-d.done
		if assert.True(t, msg) {
			done <- true
		}
	}()

	d.moveSteps(400)
	<-done
}

func TestDisableDriver(t *testing.T) {
	d, pins := mockDriver(t, false)

	if d.enableInv {
		pins["ena"].EXPECT().Low().Times(0)
	} else {
		pins["ena"].EXPECT().High().Times(1)
	}
	d.disableMotor()
}

func TestMoveRevolutions(t *testing.T) {
	pulses := revDegCounter(t, 2.5, true)
	assert.EqualValues(t, 1000, pulses)
	pulses = revDegCounter(t, 0.6, true)
	assert.EqualValues(t, 240, pulses)
}

func TestMoveDegrees(t *testing.T) {
	pulses := revDegCounter(t, 120, false)
	assert.EqualValues(t, 133, pulses)
	pulses = revDegCounter(t, 720, false)
	assert.EqualValues(t, 800, pulses)
}

func TestDirectionMotor(t *testing.T) {
	d, pins := mockDriver(t, true)

	gomock.InOrder(
		pins["dir"].EXPECT().High().Times(1),
		pins["dir"].EXPECT().Low().Times(1),
	)

	d.directionMotor(forward)
	d.directionMotor(backward)
}
