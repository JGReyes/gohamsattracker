/*
File: board.go
Project: GoHAMSatTracker
Created Date: Thursday, September 22nd 2022, 3:25:15 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

package board

import (
	"log"

	"github.com/stianeikeland/go-rpio/v4"
	"gobot.io/x/gobot/drivers/i2c"
	"gobot.io/x/gobot/platforms/raspi"
)

var adc *i2c.ADS1x15Driver
var motorEl, motorAz, motorRo *A4988
var raspberryW *raspi.Adaptor

// La interfaz y estructuras más abajo emulan una union de C.
// La cantidad a mover un motor puede ser pasos o vueltas o grados.
type amount interface {
	isAmount()
}

type nsteps struct {
	steps int
}

func (s nsteps) isAmount() {}

type nrevolutions struct {
	revolutions float32
}

func (r nrevolutions) isAmount() {}

type ndegrees struct {
	degrees float32
}

var enableMotorEl, enableMotorAz, enableMotorRo bool

func (d ndegrees) isAmount() {}

// init Inicializa y configura los drivers para los motores y el ADC.
// Se le indican los pasos por vuelta de los tres motores: elevación, azimut y rotación,
// las velocidades, si activarlos y si los pines de activación (señal enable) está invertidos.
func Config(stepsPREl, stepsPRAz, stepsPRRo, speedEl, speedAz, speedRo uint16,
	enableEl, enableAz, enableRo, enableInvEl, enableInvAz, enableInvRo bool) {

	raspberryW = raspi.NewAdaptor()
	adc = i2c.NewADS1015Driver(raspberryW)
	adc.DefaultGain, _ = adc.BestGainForVoltage(3.3)
	adc.DefaultDataRate = 128
	adc.WithAddress(0x48)
	adc.Start()

	const sleep = 9
	const ms1 = 22
	const ms2 = 23
	const ms3 = 24

	motorEl = newDriverA4988(rpio.Pin(4), rpio.Pin(sleep),
		rpio.Pin(16), rpio.Pin(25), rpio.Pin(ms1), rpio.Pin(ms2), rpio.Pin(ms3),
		halfStep, stepsPREl, enableInvEl, rpio.Open)

	motorAz = newDriverA4988(rpio.Pin(5), rpio.Pin(sleep),
		rpio.Pin(17), rpio.Pin(26), rpio.Pin(ms1), rpio.Pin(ms2), rpio.Pin(ms3),
		halfStep, stepsPRAz, enableInvAz, rpio.Open)

	motorRo = newDriverA4988(rpio.Pin(6), rpio.Pin(sleep),
		rpio.Pin(18), rpio.Pin(27), rpio.Pin(ms1), rpio.Pin(ms2), rpio.Pin(ms3),
		halfStep, stepsPRRo, enableInvRo, rpio.Open)

	motorEl.setSpeed(speedEl)
	motorAz.setSpeed(speedAz)
	motorRo.setSpeed(speedRo)

	enableMotorEl = enableEl
	enableMotorAz = enableAz
	enableMotorRo = enableRo

	if enableEl {
		motorEl.enableMotor()
	} else {
		motorEl.disableMotor()
	}

	if enableAz {
		motorAz.enableMotor()
	} else {
		motorAz.disableMotor()
	}

	if enableRo {
		motorRo.enableMotor()
	} else {
		motorRo.disableMotor()
	}
}

// MoveMotor Se encarga de mover el motor indicado la cantidad de pasos,
// vueltas o grados indicados.
func MoveMotor(motor string, number amount) {

	switch motor {
	case "elevation":
		if !enableMotorEl {
			return
		}
		switch num := number.(type) {
		case nsteps:
			motorEl.moveSteps(num.steps)
		case nrevolutions:
			motorEl.moveRevolutions(num.revolutions)
		case ndegrees:
			motorEl.moveDegrees(num.degrees)
		}
	case "azimuth":
		if !enableMotorAz {
			return
		}
		switch num := number.(type) {
		case nsteps:
			motorAz.moveSteps(num.steps)
		case nrevolutions:
			motorAz.moveRevolutions(num.revolutions)
		case ndegrees:
			motorAz.moveDegrees(num.degrees)
		}
	case "rotation":
		if !enableMotorRo {
			return
		}
		switch num := number.(type) {
		case nsteps:
			motorRo.moveSteps(num.steps)
		case nrevolutions:
			motorRo.moveRevolutions(num.revolutions)
		case ndegrees:
			motorRo.moveDegrees(num.degrees)
		}
	}
}

// MoveElMotorSteps mueve el motor de elevación los pasos indicados.
func MoveElMotorSteps(steps int) {
	MoveMotor("elevation", nsteps{steps: steps})
}

// MoveAzMotorSteps mueve el motor de azimut los pasos indicados.
func MoveAzMotorSteps(steps int) {
	MoveMotor("azimuth", nsteps{steps: steps})
}

// MoveRoMotorSteps mueve el motor de rotación los pasos indicados.
func MoveRoMotorSteps(steps int) {
	MoveMotor("rotation", nsteps{steps: steps})
}

// MoveElMotorRevs mueve el motor de elevación las vueltas indicadas.
func MoveElMotorRevs(revolutions float32) {
	MoveMotor("elevation", nrevolutions{revolutions: revolutions})
}

// MoveAzMotorRevs mueve el motor de azimut las vueltas indicadas.
func MoveAzMotorRevs(revolutions float32) {
	MoveMotor("azimuth", nrevolutions{revolutions: revolutions})
}

// MoveRoMotorRevs mueve el motor de rotación las vueltas indicadas.
func MoveRoMotorRevs(revolutions float32) {
	MoveMotor("rotation", nrevolutions{revolutions: revolutions})
}

// MoveElMotorDegrees mueve el motor de elevación los grados indicados.
func MoveElMotorDegrees(degrees float32) {
	MoveMotor("elevation", ndegrees{degrees: degrees})
}

// MoveAzMotorDegrees mueve el motor de azimut los grados indicados.
func MoveAzMotorDegrees(degrees float32) {
	MoveMotor("azimuth", ndegrees{degrees: degrees})
}

// MoveRoMotorDegrees mueve el motor de rotación los grados indicados.
func MoveRoMotorDegrees(degrees float32) {
	MoveMotor("rotation", ndegrees{degrees: degrees})
}

// HaltElMotor para el motor de elevación.
func HaltElMotor() {
	motorEl.haltMotor()
}

// HaltAzMotor para el motor de azimut.
func HaltAzMotor() {
	motorAz.haltMotor()
}

// HaltRoMotor para el motor de rotación.
func HaltRoMotor() {
	motorRo.haltMotor()
}

// HaltAllMotors para todos los motores.
func HaltAllMotors() {
	motorEl.haltMotor()
	motorAz.haltMotor()
	motorRo.haltMotor()
}

// DisableAllMotors desactiva todos los motores.
func DisableAllMotors() {
	motorEl.disableMotor()
	motorAz.disableMotor()
	motorRo.disableMotor()
}

// WaitElMotor espera a que el motor de elevación termine.
func WaitElMotor() {
	<-motorEl.done
}

// WaitAzMotor espera a que el motor de azimut termine.
func WaitAzMotor() {
	<-motorAz.done
}

// WaitRoMotor espera a que el motor de rotación termine.
func WaitRoMotor() {
	<-motorRo.done
}

// ChkBusElMtr comprueba si el motor de elevación está ocupado.
func ChkBusElMtr() (busy bool) {
	motorEl.busy.mux.Lock()
	busy = motorEl.busy.is
	motorEl.busy.mux.Unlock()
	return
}

// ChkBusAzMtr comprueba si el motor de azimut está ocupado.
func ChkBusAzMtr() (busy bool) {
	motorAz.busy.mux.Lock()
	busy = motorAz.busy.is
	motorAz.busy.mux.Unlock()
	return
}

// ChkBusRoMtr comprueba si el motor de rotación está ocupado.
func ChkBusRoMtr() (busy bool) {
	motorRo.busy.mux.Lock()
	busy = motorRo.busy.is
	motorRo.busy.mux.Unlock()
	return
}

// ReadBattVolt Devuelve el voltaje de la batería.
func ReadBattVolt() (float32, error) {
	v, err := adc.ReadDifferenceWithDefaults(0)
	if err != nil {
		log.Printf("Error reading battery voltage: %+v", err)
	}
	return float32(v * 11.1), err // 11 es el ratio del divisor de tensión + calibración (error 1%)
}
