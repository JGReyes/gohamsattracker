//go:generate  mockgen -source=stepper.go -destination=mocks/mock_stepper.go PinI
/*
File: stepper.go
Project: GoHAMSatTracker
Created Date: Monday, September 26th 2022, 10:37:17 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// A simple driver for A4988 or any driver that require pulse,
// direction and enable signals (Like the DM542T used in the project).

package board

import (
	"log"
	"math"
	"sync"
	"time"
)

type PinI interface {
	Input()
	Output()
	Low()
	High()
}

type busyT struct {
	is  bool
	mux sync.Mutex
}

type PortsOp func() error

type A4988 struct {
	enable        PinI
	sleep         PinI
	enableInv     bool
	pulse         PinI
	direction     PinI
	ms1           PinI
	ms2           PinI
	ms3           PinI
	configSteps   stepConfig
	stepsPerRev   uint16
	halt          chan bool
	done          chan bool
	busy          busyT
	speedRpm      uint16
	maxSpeedRpm   uint16
	pulseWidthUs  time.Duration
	timeBPulsesUs time.Duration
	openPorts     PortsOp
}

type motorDirection byte

const (
	forward motorDirection = iota
	backward
)

type stepConfig uint16

const (
	fullStep      stepConfig = 0b000 // 0
	halfStep                 = 0b100 // 4
	quarterStep              = 0b010 // 2
	eighthStep               = 0b110 // 6
	sixteenthStep            = 0b111 // 7
	none                     = 100
)

// init configura los pines del driver.
func (d *A4988) init() {

	err := d.openPorts()
	if err != nil {
		log.Printf("Error opening GPIO memory: %s", err)
	} else {
		d.enable.Output()
		d.sleep.Output()
		d.pulse.Output()
		d.direction.Output()
		d.ms1.Output()
		d.ms2.Output()
		d.ms3.Output()

		d.disableMotor()
		d.directionMotor(forward)
		d.pulse.Low()
		d.sleep.High()

		switch d.configSteps {
		case fullStep:
			d.ms1.Low()
			d.ms2.Low()
			d.ms3.Low()
		case halfStep:
			d.ms1.High()
			d.ms2.Low()
			d.ms3.Low()
		case quarterStep:
			d.ms1.Low()
			d.ms2.High()
			d.ms3.Low()
		case eighthStep:
			d.ms1.High()
			d.ms2.High()
			d.ms3.Low()
		case sixteenthStep:
			d.ms1.High()
			d.ms2.High()
			d.ms3.High()
		case none:

		}
	}

	d.halt = make(chan bool)
	d.done = make(chan bool)
	// Fija el ancho mínimo de pulso que los drivers pueden detectar.
	d.pulseWidthUs = 5
	// Fija el tiempo mínimo entre pulsos. O tiempo mínimo para detectar el siguiente pulso
	d.timeBPulsesUs = 5
	// Con 10 Us de pulso en total se limita la frecuencia a 100 Khz, que a
	// 400 pulsos por vuelta (half step) serían 15000 rpm
	// y a 3200 pulsos por vuelta (16 microsteep) serían 1875 rpm

	// Un motor de pasos pierde torque rápidamente con la velocidad, por lo
	// que se limita la velocidad máxima a 300 rpm
	d.maxSpeedRpm = 300
	// La velocidad por defecto se fija en 10 rpm
	d.setSpeed(10)
	d.busy = busyT{is: false, mux: sync.Mutex{}}
}

// NewDriverA4988 crea un nuevo driver para un A4988 o uno genérico si se
// indica None en la configuración de pasos.
//
// Para un A4988 indicar los pasos por vuelta (stepsRevol) y la configuración
// de micropasos (steps). Al inizializar el driver calculará los pasos totales
// y sustituirá stepsRevol.
//
// Si es un driver genérico (steps en None), indicar los pasos totales incluyendo
// los micropasos, para una vuelta completa en stepsRevol. El pin sleep no se utilizará.
//
// Por defecto la señal enable del driver se considera 0 (driver activado) y
// 1 para desactivar el driver. En caso contrario poner enableInv a true.
func newDriverA4988(enable, sleep, pulse, direction, ms1, ms2, ms3 PinI,
	steps stepConfig, stepsRevol uint16, enableInv bool, ports PortsOp) *A4988 {

	driver := A4988{enable: enable, sleep: sleep, pulse: pulse, direction: direction,
		ms1: ms1, ms2: ms2, ms3: ms3, configSteps: steps, stepsPerRev: stepsRevol,
		enableInv: enableInv, openPorts: ports}

	driver.init()

	return &driver
}

// Disable desactiva el driver.
func (d *A4988) disableMotor() {
	if d.enableInv {
		d.enable.Low()
	} else {
		d.enable.High()
	}
}

// Enable activa el driver.
func (d *A4988) enableMotor() {
	if d.enableInv {
		d.enable.High()
	} else {
		d.enable.Low()
	}
}

// Halt manda la señal para parar el motor.
func (d *A4988) haltMotor() {
	select {
	case d.halt <- true:
	case <-time.After(150 * time.Millisecond):
	}
}

// GetSpeed devuelve la velocidad del motor seleccionada en rpm.
func (d *A4988) getSpeed() uint16 {
	return d.speedRpm
}

// SetMaxRpm limita la velocidad máxima del motor. (0 para máxima velocidad)
func (d *A4988) setMaxRpm(rpm uint16) {
	if rpm < d.getMaxSpeed() {
		d.maxSpeedRpm = rpm
	}
}

// SetSpeed fija la velocidad en rpm
func (d *A4988) setSpeed(rpm uint16) {
	maxRpm := d.getMaxSpeed()
	// Comprueba que no se excede la velocidad máxima teniendo en cuenta el ancho
	// de pulso mínimo.
	if rpm > maxRpm {
		d.speedRpm = maxRpm
	} else {
		d.speedRpm = rpm
	}

	// Comprueba además si existe una restricción manual de RPM (tiene preferencia)
	if d.maxSpeedRpm > 0 {
		if rpm < d.maxSpeedRpm {
			d.speedRpm = rpm
		} else {
			d.speedRpm = d.maxSpeedRpm
		}
	}
}

// getMaxSpeed devuelve las RPM máximas del motor según el ancho de pulso,
// el tiempo mínimo entre pulsos  y el número de pulsos por vuelta.
func (d *A4988) getMaxSpeed() uint16 {
	return uint16(uint32(60*1000000) / (uint32(d.stepsPerRev) * uint32(d.pulseWidthUs+
		d.timeBPulsesUs)))
}

// getStepDelay devuelve el tiempo entre pulsos en nanosegundos, teniendo en
// cuenta la velocidad en RPM y los pasos por vuelta del motor.
func (d *A4988) getStepDelay() time.Duration {
	return time.Duration(int64(60*1000000000) / (int64(d.stepsPerRev) * int64(d.speedRpm)))
}

// MoveSteps mueve el motor los pasos indicados.
func (d *A4988) moveSteps(steps int) {

	// En caso de otra función llamar a ésta con 0 pasos o estár ya en movimiento,
	// evita los cálculos y lanzar la go rutina.
	d.busy.mux.Lock()
	busy := d.busy.is
	d.busy.mux.Unlock()

	if steps == 0 || busy {
		return
	}

	var stepsToMove uint

	if steps >= 0 {
		d.directionMotor(forward)
		stepsToMove = uint(steps)
	} else {
		d.directionMotor(backward)
		stepsToMove = uint(-steps)
	}

	go func() {
		d.busy.mux.Lock()
		d.busy.is = true
		d.busy.mux.Unlock()

		var currentStep uint = 0
		var pastT, now int64

		// Se añade una rutina simple para acelerar y decelerar durante el 20%
		// de los primeros y últimos pasos.
		var constant, stepDone bool = false, false
		accelSteps := uint(float32(stepsToMove) * float32(0.20))
		constSteps := stepsToMove - accelSteps
		deltaPStep := d.getStepDelay() * time.Duration(accelSteps)
		// No se acelera/desacelera por intervalos superiores a medio segundo
		if deltaPStep > 500*time.Millisecond {
			accelSteps = uint(math.Round(float64((500 * time.Millisecond) / d.getStepDelay())))
			deltaPStep = d.getStepDelay() * time.Duration(accelSteps)
			constSteps = stepsToMove - accelSteps
		}
		accelTicker := time.NewTicker(d.getStepDelay() / 2)
		speedTicker := time.NewTicker(deltaPStep + 1)

		//log.Printf("accelSteps = %v", accelSteps)
		//log.Printf("constSteps = %v", constSteps)
		//log.Printf("deltaPStep = %v", deltaPStep)

		for {
			select {
			case <-accelTicker.C:
				if stepDone {
					//log.Printf("currentStep = %v", currentStep)
					stepDone = false
					if currentStep <= accelSteps && deltaPStep > 0 {
						speedTicker.Stop()
						speedTicker = time.NewTicker(deltaPStep / time.Duration(currentStep+1))
						//log.Println("accell+")

					} else if currentStep <= constSteps && !constant {
						speedTicker.Stop()
						// Si avanza muy pocos pasos, la velocidad se reduce por 4.
						if deltaPStep == 0 {
							speedTicker = time.NewTicker(d.getStepDelay() * 4)
						} else {
							speedTicker = time.NewTicker(d.getStepDelay())
						}
						constant = true
						//log.Println("constant speed")

					} else if currentStep > constSteps && deltaPStep > 0 {
						speedTicker.Stop()
						lastStep := stepsToMove - currentStep
						if lastStep == 0 {
							lastStep = 1
						}
						speedTicker = time.NewTicker(deltaPStep / time.Duration(lastStep))
						//log.Println("accell-")
						//log.Printf("lastStep = %v", lastStep)
					}
				}
			case <-d.halt:
				speedTicker.Stop()
				accelTicker.Stop()
				d.busy.mux.Lock()
				d.busy.is = false
				d.busy.mux.Unlock()
				d.done <- false
				return
			// No es determinista, por lo que las RPMs serán aproximadas y
			// variables según la carga del procesador. Pero no importa para
			// esta aplicación.
			// Limitadas a unas 20 RPMs a 3200 pasos por vuelta, para más
			// velocidad habría que compilar un kernel "Real Time" y aumentar
			// el "Tick" del sistema (CONFIG_HZ) a 1000 HZ. O usar drivers
			// de menos pasos por vuelta.
			case <-speedTicker.C:
				if currentStep < stepsToMove {
					pastT = time.Now().UnixMicro()
					d.pulse.High()

					// Bloquea, pero es un 80% más rápido que usar time.Sleep
					// y ronda los 5us en vez de los 30us.
					for {
						now = time.Now().UnixMicro()
						if now-pastT >= int64(d.pulseWidthUs) {
							d.pulse.Low()
							break
						}
					}
					// La próxima llamada va ha ser superior a d.timeBPulsesUs.
					currentStep++
					stepDone = true
				} else {
					speedTicker.Stop()
					accelTicker.Stop()
					d.busy.mux.Lock()
					d.busy.is = false
					d.busy.mux.Unlock()
					d.done <- true
					return
				}
			}
		}
	}()
}

// MoveRevolutions mueve el motor las vueltas indicadas.
func (d *A4988) moveRevolutions(revolutions float32) {
	steps := int(math.Round(float64(revolutions) * float64(d.stepsPerRev)))
	d.moveSteps(steps)
}

// MoveDegrees mueve el motor el número de grados especificado.
func (d *A4988) moveDegrees(degrees float32) {
	revs := degrees / 360
	d.moveRevolutions(revs)
}

// Direction indica en que dirección debe rotar el motor.
func (d *A4988) directionMotor(dir motorDirection) {
	switch dir {
	case forward:
		d.direction.High()
	case backward:
		d.direction.Low()
	}
}
