/*
File: confvalidator.go
Project: GoHAMSatTracker
Created Date: Thursday, June 16th 2022, 1:38:32 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene los validadores de la aplicación
package validator

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/data/validation"
)

// Config contiene los validadores de la configuración de la aplicación.
type config struct {
	URL       fyne.StringValidator
	Int       fyne.StringValidator
	Float     fyne.StringValidator
	Degrees   fyne.StringValidator
	IPOrHost  fyne.StringValidator
	Latitude  fyne.StringValidator
	Longitude fyne.StringValidator
	Rpm       fyne.StringValidator
	Uint16    fyne.StringValidator
}

var Config = config{
	URL: validation.NewRegexp(`https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)`,
		"Invalid URL / URL Inválida"),
	Int: newNotCeroExp(`^\d+$`,
		"Invalid integer number / Número entero no válido"),
	Degrees: newNotCeroExp("^(-[1-9]|-?[1-9]\\d|-?[12]\\d{2}|-?3[0-5]\\d|-?360|\\d)$",
		"Invalid degrees number (-360/360) / Número de grados no válido (-360/360)"),
	Float: newNotCeroExp(`^[+-]?([0-9]+[.])[0-9]+$`,
		"Invalid float number / Número decimal no válido"),
	IPOrHost: newIpOrHostExp(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`,
		`^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$`,
		"Invalid IP/Host name (IP o nombre no válido)"),
	Latitude: validation.NewRegexp(`^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$`,
		"Invalid Latitude / Latitud no válida"),
	Longitude: validation.NewRegexp(`^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$`,
		"Invalid Longitude / Longitud no válida"),
	Rpm: newRpmExp(`^\d+$`, 1000,
		"Invalid Rpm number or too high / Rpm no válidas o muy altas"),
	Uint16: newNotCeroExp(`^([1-9]|[1-9]\d{1,3}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])$`,
		"Invalid number (1-65535) / Número no válido (1-65535)"),
}

// SetMaxRpm vuelve a definir el validador de rpm con el nuevo límite indicado.
func SetMaxRpm(rpm uint16) {
	Config.Rpm = newRpmExp(`^\d+$`, rpm,
		"Invalid Rpm number or too high / Rpm no válidas o muy altas")
}

// newIpOrHostexp comprueba si un texto es una ip o nombre de host válido,
// en caso negativo devuelve un error con una razón (reason).
func newIpOrHostExp(ipExp, hostExp, reason string) fyne.StringValidator {
	regStr := []string{ipExp, hostExp}
	regExps := make([]*regexp.Regexp, len(regStr))
	numbers := regexp.MustCompile(`^([0-9]+[.]?)+$`)

	// Comprueba que las expresiones dadas se pueden compilar.
	for i, exp := range regStr {
		expression, err := regexp.Compile(exp)
		if err != nil {
			fyne.LogError(fmt.Sprintf("Regexp did not compile expresion with index %d", i), err)
			return nil
		}
		regExps[i] = expression
	}

	err := errors.New(reason)
	return func(text string) error {
		if numbers.MatchString(text) { // Si son números y puntos será una IP.
			if regExps[0] != nil && regExps[0].MatchString(text) {
				return nil
			}
		} else { // Si no será un nombre de host.
			if regExps[1] != nil && regExps[1].MatchString(text) {
				return nil
			}
		}

		return err
	}
}

// newRpmexp comprueba si un texto es una velocidad en rpm válida,
// en caso negativo devuelve un error con una razón (reason).
func newRpmExp(rpmExp string, maxRpm uint16, reason string) fyne.StringValidator {

	// Comprueba que la expresión dada se pueden compilar.
	expression, err := regexp.Compile(rpmExp)
	if err != nil {
		fyne.LogError(fmt.Sprintln("Regexp did not compile expresion"), err)
		return nil
	}

	err = errors.New(reason)
	return func(text string) error {
		if expression.MatchString(text) { // Si son números enteros mayores a 0
			if rpm, err := strconv.ParseUint(text, 10, 16); err == nil {
				if uint16(rpm) <= maxRpm && uint16(rpm) != 0 {
					return nil
				}
			}
		}
		return err
	}
}

func newNotCeroExp(exp string, reason string) fyne.StringValidator {

	expression, err := regexp.Compile(exp)
	if err != nil {
		fyne.LogError(fmt.Sprintln("Regexp did not compile expresion"), err)
		return nil
	}

	ceros := regexp.MustCompile(`^0+$|^0.0+$`) // solo ceros
	err = errors.New(reason)
	return func(text string) error {
		if ceros.MatchString(text) {
			return err
		}
		if expression.MatchString(text) {
			return nil
		}
		return err
	}
}
