/*
File: core_test.go
Project: GoHAMSatTracker
Created Date: Friday, July 8th 2022, 4:00:19 pm
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

package core

import (
	"os"
	"testing"

	"fyne.io/fyne/v2/app"
	"github.com/stretchr/testify/assert"
)

func TestDownloadTLE(t *testing.T) {

	url := "http://www.amsat.org/amsat/ftp/keps/current/nasa.all"
	dir := "../../.."

	filename, errx := DownloadTLE(url, dir)

	assert.DirExists(t, dir)
	if assert.NotEmpty(t, filename) {
		if assert.FileExists(t, filename) {
			os.Remove(filename)
		}
	}
	assert.Nil(t, errx)
}

func TestDownloadTLE_InvalidUrl(t *testing.T) {
	url := "http://www.amsat.org/invalidurl/nasa.all"
	dir := "../../.."

	assert.DirExists(t, dir)
	assert.NoFileExists(t, dir+"/nasa.all")

	filename, errx := DownloadTLE(url, dir)

	if !assert.Empty(t, filename) {
		if !assert.NoFileExists(t, filename) {
			os.Remove(filename)
		}
	}

	if assert.Error(t, errx) {
		assert.True(t, errx.IsOfType(ConnectError))
	}
}

func TestGetTLE(t *testing.T) {
	app.New()
	dirUri := "file://../../resource/tests"

	line1, line2 := getTLE("AO-07", dirUri+"/nasa.all")

	assert.Equal(t, "1 07530U 74089B   22188.54104284 -.00000029  00000-0  10357-3 0  9993", line1)
	assert.Equal(t, "2 07530 101.9048 168.4749 0012358   5.5276 108.6298 12.53655723179980", line2)

	line1, line2 = getTLE("HUBBLE", dirUri+"/nasa.all")

	assert.Equal(t, "1 20580U 90037B   22188.33037000  .00001415  00000-0  72628-4 0  9991", line1)
	assert.Equal(t, "2 20580  28.4709  37.0879 0002358 248.6535 282.4652 15.10634529569221", line2)

}

func TestGetTLE_InvalidUri(t *testing.T) {
	app.New()
	dirUri := "file://../../resource/tests"

	line1, line2 := getTLE("AO-07", dirUri+"/noExist.txt")

	assert.Empty(t, line1)
	assert.Empty(t, line2)

	dirUri = "file://../../dontExist"

	line1, line2 = getTLE("AO-07", dirUri+"/nasa.all")

	assert.Empty(t, line1)
	assert.Empty(t, line2)

	dirUri = "../../dontExist"

	line1, line2 = getTLE("AO-07", dirUri+"/nasa.all")

	assert.Empty(t, line1)
	assert.Empty(t, line2)

}

func TestGetAllSat(t *testing.T) {
	app.New()
	uri := "file://../../resource/tests/nasa.all"

	expNames := []string{"AO-07", "UO-11", "LO-19", "HUBBLE", "MET-2/21",
		"AO-27", "IO-26", "FO-29", "NOAA-15", "GO-32", "ISS", "NO-44", "SO-50",
		"CO-55", "CO-57", "RS-22", "NOAA-18", "CO-58", "Falconsat-3", "CO-65",
		"AAUSAT2", "DO-64", "CO-66", "RS-30", "PRISM", "NOAA-19", "SRMSAT",
		"HRBE", "RS-40", "ZACUBE-1", "Delfi-N3xt", "GOMX-1", "LO-74", "AO-73",
		"SPROUT", "DTUSAT-2", "EO-80", "HODOYOSHI-1", "FIREBIRD FU3",
		"FIREBIRD FU4", "GRIFEX", "XW-2A", "XW-2C", "XW-2D", "LILACSAT-2",
		"XW-2E", "XW-2F", "XW-2B", "DCBB", "TW-1A", "IO-86", "AO-85",
		"ATHENOXAT-1", "ChubuSat-2", "ChubuSat-3", "Horyu-4", "e-st@r-II",
		"AAUSAT4", "LO-87", "NuSat2", "Lapan A3", "Pratham", "TY-1", "CAS-2T",
		"EO-88", "CAS-4B", "CAS-4A", "NIUSat", "Aalto 1", "URSA MAIOR",
		"Max Valier Sat", "Pegasus", "NUDATSat", "SUCHAI 1", "SKCUBE",
		"VZLUSAT 1", "VENTA 1", "Robusta 1B", "D-SAT", "TechnoSat", "QIKCOM-1",
		"AO-91", "PICSAT", "AO-92", "TianYi 2", "Zhou Enlai", "TianYi 6",
		"S-Net D", "S-Net B", "S-Net A", "S-Net C", "FMN 1", "Shaonian Xing",
		"SurfSat", "CP-7 DAVE", "ELFIN-B", "ELFIN-A", "CubeBel-1",
		"Changshagaoxin", "Ten-Koh", "PO-101", "IRVINE 01", "QO-100",
		"Reaktor Hello World", "MinXSS-2", "AO-95", "RANGE-B", "RANGE-A",
		"MOVE-II", "SNUSat 2", "SnugLite", "ITASat 1", "EAGLET 1", "ESEO",
		"CSIM", "Astrocast 0.1", "JO-97", "Suomi 100", "Al Farabi 2", "CHOMPTT",
		"ISAT", "UWE-4", "Sparrow", "Lume 1", "FO-99", "MYSat 1", "AstroCast 0.2",
		"AISTechSat 3", "AISat 1", "Prox-1", "Oculus-ASR", "Armadillo", "NO-104",
		"Move-IIb", "SONATE", "BeeSat 9", "JAISAT-1", "LightSail-2", "Taurus 1",
		"Duchifat 3", "OPS-SAT", "TO-108", "FloripaSat 1", "RS-44", "BY70-2",
		"UPMSat 2", "AmicalSat", "SALSAT", "BY70-3", "CAPE 3", "AO-109",
		"MiTEE 1", "ExoCube 2", "UVSQ-SAT", "YUSAT 1", "SOMP 2b", "IDEASSat",
		"Maya-2", "CubeSX-Sirius-HSE", "CubeSX-HSE", "KSU-Cubesat", "GRBAlpha",
		"Orbicraft-Zorkiy", "STECCO", "DO-111", "MO-110", "FEES", "RamSat",
		"TUBIN", "IT-SPINS", "LEDSAT", "CUTE", "Binar-1", "Maya-3", "Maya-4",
		"CUAVA-1", "TeikyoSat 4", "Z-Sat", "KOSEN 1", "HO-113", "Tevel",
		"Grizu-263a", "NO-116", "IRIS-A", "DELFI-PQ", "SO-115", "SO-114",
		"OreSat0", "Planetum 1", "Foresail-1", "SNUGLITE 2"}

	names := GetAllSat(uri)
	assert.Equal(t, expNames, names)
}

func TestSetSat(t *testing.T) {
	app.New()
	uri := "file://../../resource/tests/nasa.all"
	resp := SetSat("AO-07", uri)

	assert.True(t, resp)
}

func TestSetSat_InvalidData(t *testing.T) {
	app.New()
	uri := "file://../../resource/tests/noExist.all"

	assert.False(t, SetSat("AO-07", uri))

	uri = "file://../../resource/tests/nasa.all"

	assert.False(t, SetSat("AO-99", uri))
}

func TestSatPeriod(t *testing.T) {
	period := SatPeriod(10.0)
	expected := 84.63332248888726
	assert.EqualValues(t, expected, period)

	// Órbita baja. Ejemplo: NASA Aqua satellite
	period = SatPeriod(705.0)
	expected = 98.83025631165411
	assert.EqualValues(t, expected, period)

	// Órbita media. Ejemplo: GPS satellite
	period = SatPeriod(20350.0)
	expected = 725.2481844168976
	assert.EqualValues(t, expected, period)

	// Órbita alta. Ejemplo: Weather satellite
	period = SatPeriod(35786.0)
	expected = 1437.1829550870148
	assert.EqualValues(t, expected, period)
}
