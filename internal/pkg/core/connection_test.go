/*
File: connection_test.go
Project: GoHAMSatTracker
Created Date: Monday, October 3rd 2022, 11:29:50 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

# MIT License

# Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/
package core

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/fxamacker/cbor/v2"
	"github.com/joshuaferrara/go-satellite"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
)

var ns *server.Server

func startServer() {
	opts := &server.Options{Host: "127.0.0.1", Port: 4223,
		Username: "gohamsattraker", Password: "$2a$11$R3bIaPh3cedNgf/HZBK7yu/zCWC2uIzGSAwhtufrULnsE9u09ekuO"}

	var err error
	ns, err = server.NewServer(opts)

	if err != nil {
		fmt.Printf("Error in server initialization: %+v", err)
		panic(err)
	} else {
		go ns.Start()
		ns.ReadyForConnections(5 * time.Second)
	}

}

func stopServer() {
	ns.Shutdown()
}

func commonTestSubs(t *testing.T, antFunc func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup),
	cliFunc func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup)) {

	client := make(chan string)
	antenna := make(chan string)
	var waitGr sync.WaitGroup

	startServer()
	waitGr.Add(1)
	go antFunc(antenna, client, &waitGr)
	waitGr.Add(1)
	go cliFunc(client, antenna, &waitGr)
	waitGr.Wait()
	stopServer()
	close(antenna)
	close(client)
}

func TestConnect(t *testing.T) {
	startServer()
	conn := Connect("127.0.0.2")
	assert.Nil(t, conn)
	assert.Nil(t, Subs)

	conn = Connect("127.0.0.1")
	assert.NotNil(t, conn)
	assert.NotNil(t, Subs)

	stopServer()
	conn = Connect("127.0.0.1")
	assert.Nil(t, conn)
}

func TestSubMotors(t *testing.T) {

	ant := func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")

		sub := Subscribe(Subs.Motors, func(msg *nats.Msg) {
			signal := string(msg.Data)
			if assert.Equal(t, "STOP", signal) {
				wg.Done()
			}
		})
		assert.NotNil(t, sub)
		cli <- "start send"
		wg.Wait()
	}

	cli := func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		<-cli
		Publish(Subs.Motors, []byte("STOP"), "")
		wg.Done()
	}

	commonTestSubs(t, ant, cli)

}

func TestSubPreferences(t *testing.T) {
	p := PrefsCBOR{Interv: 100, ObsLatitude: 1.0, ObsLongitude: 2.0,
		ObsElevation: 3, MinRangeEl: 4, MaxRangeEl: 5, StepsRevEl: 6, GearsMulEl: 7.0,
		MinRangeAz: 8, MaxRangeAz: 9, StepsRevAz: 10, GearsMulAz: 11.0, MinRangeRo: 12,
		MaxRangeRo: 13, StepsRevRo: 14, GearsMulRo: 15.0, EnableAz: true, EnableEl: true,
		EnableRo: false, EnableInvAz: false, EnableInvEl: true, EnableInvRo: false,
		SpeedAz: 160, SpeedEl: 170, SpeedRo: 180, TLEline1: "line1", TLEline2: "line2",
		Time: time.Now().Format(time.RFC3339), BackLsEl: 10, BackLsAz: 20, BackLsRo: 30}

	ant := func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		sub := Subscribe(Subs.Preferences, func(msg *nats.Msg) {
			var data PrefsCBOR
			cbor.Unmarshal(msg.Data, &data)

			if assert.EqualValues(t, p, data) {
				_, err := time.Parse(time.RFC3339, data.Time)
				if assert.Nil(t, err) {
					wg.Done()
				}
			}

		})
		assert.NotNil(t, sub)
		cli <- "start send"
		wg.Wait()
	}

	cli := func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		<-cli
		data, _ := cbor.Marshal(p)
		Publish(Subs.Preferences, data, "")
		wg.Done()
	}

	commonTestSubs(t, ant, cli)
}

func TestSubSteps(t *testing.T) {

	ant := func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		sub := Subscribe(Subs.Steps, func(msg *nats.Msg) {
			var mSteps MotorSteps
			cbor.Unmarshal(msg.Data, &mSteps)

			if assert.EqualValues(t, MotorSteps{StepsEl: 10, StepsAz: 20, StepsRo: 30},
				mSteps) {
				wg.Done()
			}
		})
		assert.NotNil(t, sub)
		cli <- "start send"
		wg.Wait()
	}

	cli := func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		<-cli
		SendSteps(MotorSteps{StepsEl: 10, StepsAz: 20, StepsRo: 30})
		wg.Done()
	}

	commonTestSubs(t, ant, cli)
}

func TestSubBattery(t *testing.T) {

	ant := func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		<-ant
		SendBattery(22.5)
		wg.Done()
	}

	cli := func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		sub := Subscribe(Subs.Battery, func(msg *nats.Msg) {
			var batt float32
			cbor.Unmarshal(msg.Data, &batt)

			if assert.EqualValues(t, 22.5, batt) {
				wg.Done()
			}
		})
		assert.NotNil(t, sub)
		ant <- "send battery"
		wg.Wait()
	}

	commonTestSubs(t, ant, cli)
}

func TestSubTrackData(t *testing.T) {
	td := TrackingData{SatPosition: satellite.Vector3{X: 1.0, Y: 2.0, Z: 3.0},
		Gmst: 4.0, SatAngles: satellite.LookAngles{El: 10.0, Az: 20.0, Rg: 30.0}}

	ant := func(ant <-chan string, cli chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")
		<-ant
		SendTrackData(td)
		wg.Done()
	}

	cli := func(cli <-chan string, ant chan<- string, wg *sync.WaitGroup) {
		Connection = Connect("127.0.0.1")

		sub := Subscribe(Subs.TrackData, func(msg *nats.Msg) {
			var data TrackingData
			cbor.Unmarshal(msg.Data, &data)

			if assert.EqualValues(t, td, data) {
				wg.Done()
			}
		})
		assert.NotNil(t, sub)
		ant <- "send data"
		wg.Wait()
	}

	commonTestSubs(t, ant, cli)
}
