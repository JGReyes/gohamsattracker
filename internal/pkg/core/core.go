/*
File: core.go
Project: GoHAMSatTracker
Created Date: Friday, July 1st 2022, 10:39:27 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Contiene el núcleo de la aplicación (cálculo de posición del satélite,
// de los ángulos para apuntar hacia él y manda los datos para mover la antena.)
package core

import (
	"io"
	"log"
	"math"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"fyne.io/fyne/v2/storage"
	"github.com/cavaliergopher/grab/v3"
	"github.com/joomcode/errorx"
	"github.com/joshuaferrara/go-satellite"
)

var satFTrack satellite.Satellite

var ConnectError = errorx.NewType(errorx.CommonErrors, "Connection error")

// satUpdate actualiza la posición del satélite y angúlos de visión, dada la
// posición y elevación del observador, así como la hora y fecha actual.
func SatUpdate(obsLatitude, obsLongitude, obsElevation float64, time time.Time) (
	satPos satellite.Vector3, anglesRad satellite.LookAngles, gmst float64) {
	obsPos := satellite.LatLong{Latitude: obsLatitude, Longitude: obsLongitude}

	jday := satellite.JDay(time.Year(), int(time.Month()), time.Day(),
		time.Hour(), time.Minute(), time.Second())

	gmst = satellite.ThetaG_JD(jday)

	satPos, _ = satellite.Propagate(satFTrack, time.Year(), int(time.Month()),
		time.Day(), time.Hour(), time.Minute(), time.Second())

	// Nota: Las coordenadas del observador y los ángulos que devuelve la función
	// están en radianes.
	anglesRad = satellite.ECIToLookAngles(satPos, obsPos, obsElevation, jday)
	return
}

// downloadTLE se descarga en el directorio especificado los datos TLE de los
// satélites de radioaficionados de la página de la NASA.
func DownloadTLE(url string, dir string) (filename string, errx *errorx.Error) {
	fname, _ := storage.ParseURI(url)
	file, _ := storage.ParseURI("file://" + dir + fname.Name())
	backup, _ := storage.ParseURI("file://" + dir + "backup")

	if exist, _ := storage.Exists(file); exist {
		storage.Move(file, backup)
	}

	resp, err := grab.Get(dir, url)

	if err != nil {
		errx = ConnectError.New("Error to download TLE data. Check internet connection")
		log.Printf("Error: %+v", errx)
		storage.Move(backup, file)
		return "", errx
	}

	log.Println("File saved to ", resp.Filename)
	filename = fname.Name()
	return filename, nil
}

// readFile devuelve el contenido de un fichero o nil en caso de error.
func ReadFile(uri string) *[]byte {
	if fileUri, err := storage.ParseURI(uri); err == nil {
		if tleData, err := storage.Reader(fileUri); err != nil {
			errx := errorx.Decorate(err, "File not found")
			log.Printf("Error: %+v, opening file: %s", errx, uri)
			return nil

		} else {

			defer tleData.Close()
			data, err := io.ReadAll(tleData)
			if err != nil {
				errx := errorx.Decorate(err, "Can not read the file %s", uri)
				log.Printf("Error: %+v", errx)
				return nil
			}
			return &data
		}
	}
	return nil
}

// getTLE devuelve los datos TLE de un satélite en concreto dentro del archivo
// obtenido con 'downloadTLE()'
func getTLE(sat string, uri string) (TLEline1, TLEline2 string) {

	data := ReadFile(uri)
	if data != nil {
		pattern := regexp.MustCompile(`[\s]*?.*?` + sat + `[\t )(]*?.*?[\n\r\f\v]+?(.+?)[\n\r\f\v]+?(.+?)[\n\r\f\v]`)
		tleLines := pattern.FindAllSubmatch(*data, 2)
		if tleLines != nil {
			if len(tleLines[0]) == 3 {
				TLEline1 = string(tleLines[0][1])
				TLEline2 = string(tleLines[0][2])
			}
			return
		}
	}
	return "", ""
}

// getAllSat devuelve una lista con todos los satélites del archivo con datos TLE.
func GetAllSat(uri string) (satNames []string) {
	data := ReadFile(uri)
	if data != nil {
		// Nota: (?m) es para habilitar el multi-line. No preguntes cuantas horas
		// investigando por que no funcionaba la expresión :)
		pattern := regexp.MustCompile(`(?m)^[a-zA-Z]([ ]?[^ :]+){0,3}$`)
		names := pattern.FindAll(*data, 200)
		l := len(names)

		if l > 0 {
			strNames := make([]string, 0, l)
			for _, val := range names {
				strNames = append(strNames, string(val))
			}

			satNames = strNames
			return
		}
	}
	return nil
}

// SetSat selecciona e inicializa el satélite a seguir, indicando si ha tenido exito o no.
// file is in URI format.
func SetSat(sat string, file string) bool {
	line1, line2 := getTLE(sat, file)

	if line1 != "" && line2 != "" {
		satFTrack = satellite.TLEToSat(line1, line2, satellite.GravityWGS84)

		if satFTrack.ErrorStr == "" {
			return true
		} else {
			return false
		}
	}
	return false
}

// SetSatTLE selecciona e inicializa el satélite a seguir con solo las líneas
// TLEs previamente validadas.
func SetSatTLE(line1, line2 string) bool {
	satFTrack = satellite.TLEToSat(line1, line2, satellite.GravityWGS84)

	if satFTrack.ErrorStr == "" {
		return true
	} else {
		return false
	}
}

// GetTLELines devuelve las líneas TLE del satélite ya inicializado correctamente o
// cadenas vacías en caso de no estarlo.
func GetTLELines() (TLE1, TLE2 string) {
	if satFTrack.ErrorStr == "" {
		TLE1 = satFTrack.Line1
		TLE2 = satFTrack.Line2
	} else {
		TLE1 = ""
		TLE2 = ""
	}
	return
}

// LatLongFromVector devuelve las coordenadas (grados decimales), altitud y
// velocidad de un satélite, dadas su posición en Vector3 y el tiempo en GMST.
func LatLongFromVector(pos satellite.Vector3, gmst float64) (altitude float64,
	velocity float64, position satellite.LatLong) {

	altitude, velocity, position = satellite.ECIToLLA(pos, gmst)
	position = satellite.LatLongDeg(position)
	return
}

// SatPeriod calcula el período de un satélite alrededor de la Tierra.
func SatPeriod(altitudKm float64) (minutes float64) {
	const earthRadius = 6371.0088 * 1000
	const gravityAccel = 9.8
	satAlt := altitudKm * 1000

	T := ((2 * math.Pi) / earthRadius) * math.Sqrt(math.Pow(earthRadius+satAlt, 3)/gravityAccel)
	return T / 60
}

// LogToFile redirige la salida del log a un archivo.
func LogToFile(file string) {
	if fileUri, err := storage.ParseURI(file); err == nil {
		if data, err := storage.Writer(fileUri); err != nil {
			errx := errorx.Decorate(err, "Can not create the log file")
			log.Printf("Error: %+v, creating file: %s", errx, file)
			if strings.Contains(err.Error(), "no repository registered for scheme") {
				// Intenta crear el archivo sin utilizar storage.
				if logFile, err := os.Create(path.Base(fileUri.String())); err == nil {
					log.Printf("Successfully created log file without registration.")
					log.SetOutput(logFile)
				}
			}
			return
		} else {
			log.SetOutput(data)
		}
	}
}

// LogToConsole devuelve la salida del log a la de por defecto.
func SetLogToDefault() {
	log.SetOutput(os.Stderr)
}
