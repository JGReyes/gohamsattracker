/*
File: connection.go
Project: GoHAMSatTracker
Created Date: Friday, September 16th 2022, 10:38:44 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

package core

import (
	"log"

	"github.com/fxamacker/cbor/v2"
	"github.com/joshuaferrara/go-satellite"
	"github.com/nats-io/nats.go"
)

var Connection *nats.Conn
var Subs *Subjects

// Subjects contiene las prosibles subscripciones, tanto del servidor como del cliente.
type Subjects struct {
	Preferences string
	Steps       string
	Battery     string
	Motors      string
	TrackData   string
}

// PrefsCBOR contiene las preferencias y TLEs pasados por el cliente.
type PrefsCBOR struct {
	_            struct{} `cbor:",toarray"`
	Interv       uint16
	ObsLatitude  float32
	ObsLongitude float32
	ObsElevation float32
	MinRangeEl   int
	MaxRangeEl   int
	StepsRevEl   uint16
	GearsMulEl   float32
	SpeedEl      uint16
	MinRangeAz   int
	MaxRangeAz   int
	StepsRevAz   uint16
	GearsMulAz   float32
	SpeedAz      uint16
	MinRangeRo   int
	MaxRangeRo   int
	StepsRevRo   uint16
	GearsMulRo   float32
	SpeedRo      uint16
	EnableAz     bool
	EnableEl     bool
	EnableRo     bool
	EnableInvAz  bool
	EnableInvEl  bool
	EnableInvRo  bool
	TLEline1     string
	TLEline2     string
	Time         string
	BackLsEl     uint16
	BackLsAz     uint16
	BackLsRo     uint16
}

type TrackingData struct {
	_           struct{} `cbor:",toarray"`
	SatPosition satellite.Vector3
	Gmst        float64
	SatAngles   satellite.LookAngles
}

// MotorSteps contiene los pasos que se quieren mover los respectivos motores.
type MotorSteps struct {
	_       struct{} `cbor:",toarray"`
	StepsEl int
	StepsAz int
	StepsRo int
}

// initSubjects inicializa los valores de las subscripciones.
func initSubjects() *Subjects {
	subs := Subjects{}
	subs.Preferences = "preferences"
	subs.Steps = "motors.steps"
	subs.Battery = "battery.voltage"
	subs.Motors = "motors.commands"
	subs.TrackData = "sat.tracking"

	return &subs
}

// Connect Realiza la conexión con el servidor, introduciendo las credenciales y puerto prefijados.
// También inicializa las posibles subscripciones.
func Connect(ip string) (conn *nats.Conn) {
	nc, err := nats.Connect("nats://gohamsattraker:gohamsattraker12345:-)@" + ip + ":4223")

	if err != nil {
		log.Printf("Error in connection: %+v", err)
		Subs = nil
	} else {
		conn = nc
		Subs = initSubjects()
		return
	}
	return nil
}

// Subscribe realiza la subscripción a un tema (subject) y llama a la función indicada
// (callback) al recibir un mensaje de ese tema.
func Subscribe(subject string, callback func(msg *nats.Msg)) *nats.Subscription {
	sub, err := Connection.Subscribe(subject, callback)

	if err != nil {
		log.Printf("Error in subscription: %+v", err)
		return nil
	} else {
		return sub
	}
}

// Publish publica un mensaje (data) en un tema dado (subject)
func Publish(subject string, data []byte, reply string) {
	msg := nats.Msg{}
	msg.Subject = subject
	msg.Reply = reply
	msg.Data = data
	err := Connection.PublishMsg(&msg)

	if err != nil {
		log.Printf("Error publishing data: %+v", err)
	}
}

// SendSteps manda los pasos a mover de cada motor a la antena.
func SendSteps(steps MotorSteps) {
	if Connection != nil {
		data, _ := cbor.Marshal(steps)
		Publish(Subs.Steps, data, "")
	}
}

// SendBattery manda el voltaje de la batería al cliente.
func SendBattery(volts float32) {
	if Connection != nil {
		data, _ := cbor.Marshal(volts)
		Publish(Subs.Battery, data, "")
	}
}

// SendTrackData manda los datos de seguimiento del satelite actual.
func SendTrackData(trkData TrackingData) {
	if Connection != nil {
		data, _ := cbor.Marshal(trkData)
		Publish(Subs.TrackData, data, "")
	}
}
