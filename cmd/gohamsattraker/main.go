//go:generate fyne bundle -o ../../internal/resource/rIcon.go --pkg resource --prefix r ../../assets/icons
//go:generate fyne bundle -o ../../internal/resource/rLangES.go --pkg resource --prefix r ../../assets/i18n/active.es.toml
/*
File: main.go
Project: GoHAMSatTracker
Created Date: Tuesday, June 7th 2022, 11:36:25 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Se encarga de realizar los cálculos para apuntar la antena al satélite seleccionado,
// mandando las instrucciones al servidor de la antena.
package main

import (
	"GoHAMSatTracker/internal/app/sattrk"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
)

func main() {
	a := app.NewWithID("gohamsattraker.net.circuiteando")
	w := a.NewWindow("GoHAMSatTracker")
	w.Resize(fyne.NewSize(360, 240))
	w.SetMaster()
	w.SetContent(sattrk.MakeUI(w))
	w.ShowAndRun()
}
