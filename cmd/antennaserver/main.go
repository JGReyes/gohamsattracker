/*
File: main.go
Project: GoHAMSatTracker
Created Date: Tuesday, June 7th 2022, 11:37:04 am
Author: JGReyes
e-mail: josegreyes@cirtuiteando.net
web: www.circuiteando.net

MIT License

Copyright (c) 2022 JGReyes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

------------------------------------
Last Modified: (INSERT DATE)         <--!!!!!!!!
Modified By: JGReyes
------------------------------------

Changes:

Date       Version   Author      Detail
(dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
*/

// Se encarga de controlar los motores de la antena. Espera comandos por red.
package main

import (
	"GoHAMSatTracker/internal/pkg/board"
	"GoHAMSatTracker/internal/pkg/core"
	"log"
	"math"
	"strconv"
	"sync"
	"time"

	"github.com/fxamacker/cbor/v2"
	"github.com/joshuaferrara/go-satellite"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
)

var prefs *core.PrefsCBOR
var prevPrefs core.PrefsCBOR
var recvTime = make(chan time.Time, 2)
var toSatAngles = make(chan satellite.LookAngles, 2)
var calcDone = make(chan bool, 1)
var trackDone = make(chan bool, 1)
var calcInProg, trackInProg, testInProg bool
var actAzDegs, actElDegs, actRoDegs float32
var oldDiffEl, oldDiffAz, oldDiffRo float32
var minAngleEl, minAngleAz, minAngleRo float32

type syncTime struct {
	time time.Time
	mux  sync.Mutex
}

type busyT struct {
	isBusy bool
	mux    sync.Mutex
}

func (b *busyT) true() {
	b.mux.Lock()
	b.isBusy = true
	b.mux.Unlock()
}

func (b *busyT) false() {
	b.mux.Lock()
	b.isBusy = false
	b.mux.Unlock()
}

func (b *busyT) is() (res bool) {
	b.mux.Lock()
	res = b.isBusy
	b.mux.Unlock()
	return
}

var azBusy, elBusy, roBusy busyT
var actualTime syncTime

func main() {
	core.LogToFile("file://./log.txt")
	opts := &server.Options{}
	err := opts.ProcessConfigFile("./natsServer.conf")

	if err != nil {
		log.Printf("Error in config file: %+v", err)
		panic(err)
	}

	ns, err := server.NewServer(opts)

	if err != nil {
		log.Printf("Error in server initialization: %+v", err)
		panic(err)
	}

	go ns.Start()

	if !ns.ReadyForConnections(10 * time.Second) {
		log.Printf("Not ready for connection.Timeout")
		panic("Not ready for connection")
	}

	core.Connection = core.Connect("127.0.0.1")

	if core.Connection != nil {
		log.Printf("Antennaserver's client connected")
	} else {
		log.Printf("Error in antennaserver's client connection")
	}

	core.Subscribe(core.Subs.Preferences, prefsReceived)
	core.Subscribe(core.Subs.Steps, controlReceived)
	core.Subscribe(core.Subs.Motors, cmdReceived)

	startClock()

	ns.WaitForShutdown()
}

func compCPrefs(data *core.PrefsCBOR) bool {
	if prevPrefs.StepsRevEl == data.StepsRevEl &&
		prevPrefs.StepsRevAz == data.StepsRevAz &&
		prevPrefs.StepsRevRo == data.StepsRevRo &&
		prevPrefs.SpeedEl == data.SpeedEl &&
		prevPrefs.SpeedAz == data.SpeedAz &&
		prevPrefs.SpeedRo == data.SpeedRo &&
		prevPrefs.EnableEl == data.EnableEl &&
		prevPrefs.EnableAz == data.EnableAz &&
		prevPrefs.EnableRo == data.EnableRo &&
		prevPrefs.EnableInvEl == data.EnableInvEl &&
		prevPrefs.EnableInvAz == data.EnableInvAz &&
		prevPrefs.EnableInvRo == data.EnableInvRo {
		return true
	} else {
		return false
	}
}

func prefsReceived(msg *nats.Msg) {
	var configData core.PrefsCBOR
	err := cbor.Unmarshal(msg.Data, &configData)

	if err != nil {
		if msg.Reply != "" {
			log.Printf("Error decoding preferences to CBOR")
		}
	} else {
		log.Printf("Received preferences")
		noChange := compCPrefs(&configData)
		prefs = &configData
		rTime, _ := time.Parse(time.RFC3339, prefs.Time)
		recvTime <- rTime
		if !noChange {
			prevPrefs = configData
			board.Config(prefs.StepsRevEl, prefs.StepsRevAz, prefs.StepsRevRo,
				prefs.SpeedEl, prefs.SpeedAz, prefs.SpeedRo, prefs.EnableEl,
				prefs.EnableAz, prefs.EnableRo, prefs.EnableInvEl, prefs.EnableInvAz,
				prefs.EnableInvRo)
		}

		if msg.Reply != "" {
			core.Publish(core.Subs.Preferences, []byte(msg.Reply), "")
		}
		if calcInProg {
			select {
			case calcDone <- true:
			case <-time.After(500 * time.Millisecond):
			}
		}
		if trackInProg {
			select {
			case trackDone <- true:
			case <-time.After(500 * time.Millisecond):
			}
		}

		initVars()
		startSatCal(calcDone, 2000)
	}
}

func initVars() {

	actAzDegs = 0.0
	actElDegs = 0.0
	actRoDegs = 0.0
	oldDiffEl = 0.0
	oldDiffAz = 0.0
	oldDiffRo = 0.0
	minAngleEl = (360.0 / float32(prefs.StepsRevEl))
	minAngleAz = (360.0 / float32(prefs.StepsRevAz))
	minAngleRo = (360.0 / float32(prefs.StepsRevRo))

	elBusy.false()
	azBusy.false()
	roBusy.false()
}

func controlReceived(msg *nats.Msg) {
	var steps core.MotorSteps
	err := cbor.Unmarshal(msg.Data, &steps)

	if err != nil {
		log.Printf("Error decoding control steps to CBOR")
	} else {
		log.Printf("Control command received")
		if steps.StepsAz != 0 {
			board.MoveAzMotorSteps(steps.StepsAz)
		}
		if steps.StepsEl != 0 {
			board.MoveElMotorSteps(steps.StepsEl)
		}
		if steps.StepsRo != 0 {
			if trackInProg { // Si está siguiendo un satélite, rota la antena los grados preestablecidos
				if roBusy.is() {
					return
				}

				if steps.StepsRo > 0 {
					antennaMoveRotation(actRoDegs + 10)
				} else {
					antennaMoveRotation(actRoDegs - 10)
				}

			} else {
				board.MoveRoMotorSteps(steps.StepsRo)
			}
		}
	}
}

func cmdReceived(msg *nats.Msg) {
	signal := string(msg.Data)

	switch signal {
	case "STOP": // Botón de parada del menú de control.
		log.Printf("Stop command received")
		stopTracking()
		board.HaltAllMotors()

	case "DISMOTORS": // Botón de apagado de motores.
		log.Printf("Disable motors command received")
		stopTracking()
		time.Sleep(1 * time.Second)
		board.DisableAllMotors()
		prevPrefs = core.PrefsCBOR{}

	case "TRACK":
		log.Printf("Start tracking command received")
		startTracking(trackDone)

	case "STOPTRACK": // Botón de seguimiento del menú principal.
		log.Printf("Stop tracking command received")
		stopTracking()

	case "INITPOS": // Devuelve la antena a su posición inicial.
		log.Printf("Motors initial position command received")
		antennaMoveAzimuth(0.0)
		antennaMoveElevation(0.0)
		antennaMoveRotation(0.0)

	case "TESTLIMITS":
		log.Printf("Test motors limits command received")
		testMotorsLimits()

	case "TEST90":
		log.Printf("Test 90 pass command received")
		test90Pass()

	case "STOPCALC":
		log.Printf("Stop calculations command received")
		if calcInProg {
			select {
			case calcDone <- true:
			case <-time.After(500 * time.Millisecond):
			}
		}
	}
}

// stopTracking detiene el hilo que mueve los motores y los para.
func stopTracking() {
	if trackInProg || testInProg {
		select {
		case trackDone <- true:
			testInProg = false
		case <-time.After(500 * time.Millisecond):
		}
		board.HaltAllMotors()
		elBusy.false()
		azBusy.false()
		roBusy.false()
	}
}

// startClock crea un reloj interno con la hora y fecha mandada por la aplicación.
func startClock() {
	go func() {
		second := time.NewTicker(1 * time.Second)
		clock := time.Time{}
		for {
			select {
			case rt := <-recvTime:
				clock = rt
			case <-second.C:
				clock = clock.Add(1 * time.Second)
				actualTime.mux.Lock()
				actualTime.time = clock
				actualTime.mux.Unlock()
			}
		}
	}()
}

// lessDiff calcula la diferencia en grados que tienen que mover la antena para un menor recorrido.
func lessDiff(wantedDegrees, actualDegrees float32) (diff float32) {
	diff = wantedDegrees - actualDegrees
	if math.Abs(float64(diff)) > 180.0 {
		diff = (360 - actualDegrees) + wantedDegrees
		if diff > 360.0 {
			diff = -((360.0 + actualDegrees) - wantedDegrees)
		}
	}
	return
}

// antennaMoveElevation mueve la antena en elevación los grados indicados, teniendo en cuenta
// el ratio de los engranajes acoplados al motor y el backlash.
func antennaMoveElevation(degrees float32) {
	//log.Printf("elDeg: %v", degrees)

	if elBusy.is() {
		return
	}

	elBusy.true()

	go func() {
		if degrees == 0.0 { // vuelve a la posición inicial
			diff := -(actElDegs)
			diff = applyBacklash(actElDegs, diff, prefs.GearsMulEl, prefs.BackLsEl, "El")
			if diff == 0 {
				elBusy.false()
				return
			}
			board.MoveElMotorDegrees(diff)
			board.WaitElMotor()
			actElDegs = 0.0
			elBusy.false()
			return
		}

		if degrees >= float32(prefs.MinRangeEl) && degrees <= float32(prefs.MaxRangeEl) {
			diff := degrees - actElDegs
			//log.Printf("elDiff: %v", diff)
			diff = applyBacklash(actElDegs, diff, prefs.GearsMulEl, prefs.BackLsEl, "El")
			//log.Printf("elDiffBl: %v", diff)
			if diff == 0 {
				elBusy.false()
				return
			}
			board.MoveElMotorDegrees(diff)
			board.WaitElMotor()
			actElDegs = degrees
		} else {
			log.Printf("Elevation angle not in " + strconv.FormatInt(int64(prefs.MinRangeEl), 10) +
				" to " + strconv.FormatInt(int64(prefs.MaxRangeEl), 10) + " range.")
		}
		elBusy.false()
	}()
}

// antennaMoveAzimuth mueve la antena en azimut los grados indicados, teniendo en cuenta
// el ratio de los engranajes acoplados al motor y el backlash.
func antennaMoveAzimuth(degrees float32) {
	//log.Printf("azDeg: %v", degrees)

	if azBusy.is() {
		return
	}

	azBusy.true()

	go func() {
		if degrees == 0.0 { // vuelve a la posición inicial
			diff := lessDiff(0.0, actAzDegs)
			diff = applyBacklash(actAzDegs, diff, prefs.GearsMulAz, prefs.BackLsAz, "Az")
			if diff == 0 {
				azBusy.false()
				return
			}
			board.MoveAzMotorDegrees(diff)
			board.WaitAzMotor()
			actAzDegs = 0.0
			azBusy.false()
			return
		}

		if degrees >= float32(prefs.MinRangeAz) && degrees <= float32(prefs.MaxRangeAz) {
			diff := lessDiff(degrees, actAzDegs)
			//log.Printf("azDiff: %v", diff)
			diff = applyBacklash(actAzDegs, diff, prefs.GearsMulAz, prefs.BackLsAz, "Az")
			//log.Printf("azDiffBl: %v", diff)
			if diff == 0 {
				azBusy.false()
				return
			}
			board.MoveAzMotorDegrees(diff)
			board.WaitAzMotor()
			actAzDegs = degrees
		} else {
			log.Printf("Azimuth angle not in " + strconv.FormatInt(int64(prefs.MinRangeAz), 10) +
				" to " + strconv.FormatInt(int64(prefs.MaxRangeAz), 10) + " range.")
		}
		azBusy.false()
	}()
}

// antennaMoveElevation rota la antena los grados indicados, teniendo en cuenta
// el ratio de los engranajes acoplados al motor y el backlash.
func antennaMoveRotation(degrees float32) {

	if roBusy.is() {
		return
	}

	roBusy.true()

	go func() {
		if degrees == 0.0 { // vuelve a la posición inicial
			diff := -(actRoDegs)
			diff = applyBacklash(actRoDegs, diff, prefs.GearsMulRo, prefs.BackLsRo, "Ro")
			if diff == 0 {
				roBusy.false()
				return
			}
			board.MoveRoMotorDegrees(diff)
			board.WaitRoMotor()
			actRoDegs = 0.0
			roBusy.false()
			return
		}

		if degrees >= float32(prefs.MinRangeRo) && degrees <= float32(prefs.MaxRangeRo) {
			diff := degrees - actRoDegs
			diff = applyBacklash(actRoDegs, diff, prefs.GearsMulRo, prefs.BackLsRo, "Ro")
			if diff == 0 {
				roBusy.false()
				return
			}
			board.MoveRoMotorDegrees(diff)
			board.WaitRoMotor()
			actRoDegs = degrees
		} else {
			log.Printf("Rotation angle not in " + strconv.FormatInt(int64(prefs.MinRangeRo), 10) +
				" to " + strconv.FormatInt(int64(prefs.MaxRangeRo), 10) + " range.")
		}
		roBusy.false()
	}()
}

// startSatCal inicia, actualiza  y manda los cálculos para el satélite recibido.
// Realizando el cálculo cada intervalo especificado en milisegundos (calcIntervalMs).
func startSatCal(done <-chan bool, calcIntervalMs uint) {

	if calcInProg {
		return
	}

	if !core.SetSatTLE(prefs.TLEline1, prefs.TLEline2) {
		return
	}

	go func() {
		calcInProg = true
		dataInteval := time.NewTicker(time.Duration(prefs.Interv) * time.Millisecond)
		calcInterval := time.NewTicker(time.Duration(calcIntervalMs) * time.Millisecond)
		var actTime time.Time
		var satPos satellite.Vector3
		var gmst float64
		var anglesRad satellite.LookAngles
		tData := core.TrackingData{}

		for {
			select {
			case <-calcInterval.C:
				actualTime.mux.Lock()
				actTime = actualTime.time
				actualTime.mux.Unlock()

				satPos, anglesRad, gmst = core.SatUpdate(float64(prefs.ObsLatitude),
					float64(prefs.ObsLongitude), float64(prefs.ObsElevation), actTime.UTC())

				select {
				case toSatAngles <- anglesRad:
				case <-time.After((time.Duration(calcIntervalMs) / 10) * time.Millisecond):
				}

			case <-dataInteval.C:
				tData.SatPosition = satPos
				tData.Gmst = gmst
				tData.SatAngles = anglesRad
				data, _ := cbor.Marshal(tData)
				core.Publish(core.Subs.TrackData, data, "")
				if battVolt, err := board.ReadBattVolt(); err == nil {
					core.SendBattery(battVolt)
				}

			case <-done:
				dataInteval.Stop()
				calcInterval.Stop()
				calcInProg = false
				return
			}
		}
	}()
}

// startTracking mueve la antena hacia el satélite.
func startTracking(done <-chan bool) {
	if !calcInProg || trackInProg {
		return
	}
	trackInProg = true

	go func() {

		// Vacía el canal de valores anteriores.
		for len(toSatAngles) > 0 {
			<-toSatAngles
		}

		for {
			select {
			case angles := <-toSatAngles:
				elDeg := angles.El * (180 / math.Pi)
				azDeg := angles.Az * (180 / math.Pi)
				antennaMoveElevation(float32(elDeg))
				antennaMoveAzimuth(float32(azDeg))

			case <-done:
				trackInProg = false
				return
			}
		}
	}()
}

// applyBacklash aplica la corrección a las holguras de los engranajes y multiplicador.
// el parámetro motor puede ser: "El", "Az" o "Ro"
func applyBacklash(actual, diff, gearMult float32, backlash uint16, motor string) (newDiff float32) {

	var minAngle, oldDiff float32

	switch motor {
	case "El":
		//log.Printf("oldDiffEl: %v", oldDiffEl)
		minAngle = minAngleEl
		oldDiff = oldDiffEl
	case "Az":
		//log.Printf("oldDiffAz: %v", oldDiffAz)
		minAngle = minAngleAz
		oldDiff = oldDiffAz
	case "Ro":
		//log.Printf("oldDiffRo: %v", oldDiffRo)
		minAngle = minAngleRo
		oldDiff = oldDiffRo
	default:
		log.Println("Error: invalid motor")

	}

	if diff == 0 || float32(math.Abs(float64(diff))) < (minAngle/gearMult) {
		newDiff = 0

	} else if (diff < 0.0) == (oldDiff < 0.0) { // si ambas posiciones tienen el mismo signo (sentido de giro)
		newDiff = diff * gearMult
		oldDiff = diff
		//log.Println("No backlash")

	} else {
		degrees := float32(backlash) * minAngle
		if diff < 0.0 { // Para ir en la misma dirección
			degrees = -(degrees)
		}
		newDiff = (diff * gearMult) + degrees
		oldDiff = diff
		//log.Println("Apply backlash")
	}

	switch motor {
	case "El":
		oldDiffEl = oldDiff
	case "Az":
		oldDiffAz = oldDiff
	case "Ro":
		oldDiffRo = oldDiff
	default:
		log.Println("Error: invalid motor")
	}
	return
}

// testMotorsLimits da una vuelta en cada sentido para probar los límites de la configuración.
func testMotorsLimits() {

	if testInProg {
		return
	}

	go func() {

		var moveFunc func(float32)
		var waitFunc func() bool
		testInProg = true

		for j := 1; j <= 3; j++ {
			switch j {
			case 1:
				moveFunc = antennaMoveAzimuth
				waitFunc = azBusy.is
			case 2:
				moveFunc = antennaMoveElevation
				waitFunc = elBusy.is
			case 3:
				moveFunc = antennaMoveRotation
				waitFunc = roBusy.is
			}

			for i := 0; i <= 360; i += 5 {
				if !testInProg {
					return
				}
				moveFunc(float32(i))
				for waitFunc() {
					time.Sleep(10 * time.Millisecond)
				}
			}

			for i := 0; i >= -360; i -= 5 {
				if !testInProg {
					return
				}
				moveFunc(float32(i))
				for waitFunc() {
					time.Sleep(10 * time.Millisecond)
				}
			}
		}

		time.Sleep(2 * time.Second)
		antennaMoveElevation(0.0)
		antennaMoveAzimuth(0.0)
		antennaMoveRotation(0.0)
		testInProg = false
	}()
}

// test90Pass simula un pasada a 90 grados de elevación.
func test90Pass() {

	if testInProg {
		return
	}

	go func() {
		var i int = 0
		type pos struct {
			az float32
			el float32
		}

		positions := [12]pos{{az: 360, el: -40}, {az: 360, el: -20},
			{az: 360, el: 0}, {az: 360, el: 20}, {az: 360, el: 45},
			{az: 360, el: 70}, {az: 360, el: 90}, {az: 180, el: 80},
			{az: 180, el: 70}, {az: 180, el: 45}, {az: 180, el: 30},
			{az: 180, el: -10}}

		testInProg = true

		for {
			if !testInProg {
				return
			}

			time.Sleep(1000 * time.Millisecond)
			antennaMoveAzimuth(positions[i].az)
			antennaMoveElevation(positions[i].el)
			for { // Espera a realizar el movimiento
				if !azBusy.is() && !elBusy.is() {
					break
				}
				time.Sleep(10 * time.Millisecond)
			}

			i++
			if i == len(positions) {
				break
			}
		}

		time.Sleep(2 * time.Second)
		antennaMoveElevation(0.0)
		antennaMoveAzimuth(0.0)
		antennaMoveRotation(0.0)
		testInProg = false
	}()
}
